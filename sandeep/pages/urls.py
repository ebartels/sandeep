from django.conf.urls import url
from sandeep.pages import views

urlpatterns = [
    url(r'^(?P<slug>[\w-]+)/$', views.detail, name='content-page-view'),
]
