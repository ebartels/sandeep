from rest_framework import serializers

from ado.apps.content.models import Page


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ('id',
                  'title',
                  'slug',
                  'text')

    def get_queryset(self):
        return Page.objects.published()
