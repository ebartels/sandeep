from rest_framework import generics

from ado.apps.content.models import Page
from sandeep.core.api.views import RetrieveCacheResponseMixin
from . import serializers


class PageDetailView(RetrieveCacheResponseMixin, generics.RetrieveAPIView):
    serializer_class = serializers.PageSerializer
    lookup_field = 'slug'

    def get_queryset(self):
        return Page.objects.filter(published=True)
