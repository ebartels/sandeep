from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^page/(?P<slug>[^/]+)/?$',
        views.PageDetailView.as_view(),
        name='page-detail'),
]
