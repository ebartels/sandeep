from django.shortcuts import render, get_object_or_404
from ado.apps.content.models import Page


def detail(request, slug):
    get_object_or_404(Page, slug=slug, published=True)
    return render(request, 'index.html')
