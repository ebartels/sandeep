from django.contrib import admin

from ado.media.admin.options import RelatedImagesInline
from .models import Exhibition


class ExhibitionImagesInline(RelatedImagesInline):
    verbose_name_plural = 'Images'
    verbose_name = 'Image'


class ExhibitionAdmin(admin.ModelAdmin):
    inlines = [ExhibitionImagesInline]
    list_display = ('__unicode__', 'year', 'position', 'published')
    list_filter = ('published', 'year')


admin.site.register(Exhibition, ExhibitionAdmin)
