from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^exhibition/?$',
        views.ExhibitionListView.as_view(),
        name='exhibition-list'),
]
