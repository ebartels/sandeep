from __future__ import unicode_literals

from rest_framework import generics
from sandeep.core.api.views import ListCacheResponseMixin

from ..models import Exhibition
from . import serializers


class ExhibitionListView(ListCacheResponseMixin, generics.ListAPIView):
    serializer_class = serializers.ExhibitionSerializer
    queryset = Exhibition.objects.filter(published=True)
