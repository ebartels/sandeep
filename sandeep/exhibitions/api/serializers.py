from __future__ import unicode_literals

from rest_framework import serializers
from sandeep.exhibitions.models import Exhibition
from sandeep.core.api.serializers import ImageSerializer


class ExhibitionSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Exhibition
        fields = [
            'title',
            'slug',
            'location',
            'year',
            'images',
        ]
