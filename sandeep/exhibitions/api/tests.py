from __future__ import unicode_literals

import factory
import factory.django
from django_webtest import WebTest
from django.core.urlresolvers import reverse


class ExhibitionFactory(factory.DjangoModelFactory):
    title = factory.Sequence(lambda n: 'Exhibition {0}'.format(n))
    slug = factory.Sequence(lambda n: 'exhibition-{0}'.format(n))
    year = 2016

    class Meta:
        model = 'exhibitions.Exhibition'


class ExhibitionListApiViewTest(WebTest):

    def setUp(self):
        self.exhibitions = [
            ExhibitionFactory(),
            ExhibitionFactory(),
            ExhibitionFactory(),
        ]

        self.url = reverse('api:exhibitions:exhibition-list')

    def test_get(self):
        resp = self.app.get(self.url, status=200)
        self.assertEqual(len(resp.json), 3)
        resp_keys = set(resp.json[0].keys())
        self.assertSetEqual(
            resp_keys,
            set([
                'title',
                'slug',
                'location',
                'year',
                'images',
            ]))

    def test_get_unpublished(self):
        ExhibitionFactory(published=False)
        resp = self.app.get(self.url, status=200)
        self.assertEqual(len(resp.json), 3)
