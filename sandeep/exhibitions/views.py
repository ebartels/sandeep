from django.views.generic import ListView, DetailView

from sandeep.exhibitions.models import Exhibition


class ExhibitionListView(ListView):
    queryset = Exhibition.objects.filter(published=True)
    template_name = 'index.html'

exhibition_list = ExhibitionListView.as_view()


class ExhibitionDetailView(DetailView):
    queryset = Exhibition.objects.filter(published=True)
    template_name = 'index.html'

exhibition_detail = ExhibitionDetailView.as_view()
