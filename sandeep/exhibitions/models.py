from django.db import models

from positions import PositionField

from ado.utils.fields import AutoSlugField
from ado.media.models import RelatedImagesField


class Exhibition(models.Model):
    title = models.CharField(max_length=255)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='title',
            help_text='Unique text identifier used in urls.')
    location = models.CharField(max_length=255, blank=True)
    published = models.BooleanField(default=True,
            help_text="Publish on site?")
    year = models.PositiveSmallIntegerField()
    position = PositionField(collection='year')
    images = RelatedImagesField()

    class Meta:
        ordering = ('-year', 'position',)

    def __unicode__(self):
        if self.location:
            return u'%s | %s' % (self.title, self.location)
        return self.title
