# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ado.media.fields.related
import ado.utils.fields
import positions.fields


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exhibition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('slug', ado.utils.fields.AutoSlugField(editable=False, prepopulate_from=b'title', max_length=200, help_text=b'Unique text identifier used in urls.', unique=True, slugify=ado.utils.fields.slugify)),
                ('location', models.CharField(max_length=255, blank=True)),
                ('published', models.BooleanField(default=True, help_text=b'Publish on site?')),
                ('year', models.PositiveSmallIntegerField()),
                ('position', positions.fields.PositionField(default=-1)),
                ('images', ado.media.fields.related.RelatedImagesField(through=b'media.ImageRelation', verbose_name=b'Images', to=b'media.Image')),
            ],
            options={
                'ordering': ('-year', 'position'),
            },
        ),
    ]
