from django.conf.urls import patterns, url

urlpatterns = patterns('sandeep.exhibitions.views',
    url(r'^$', 'exhibition_list', name='list'),
    url(r'^detail/?$', 'exhibition_list', name='detail'),
    url(r'^detail/.+/?$', 'exhibition_list', name='detail'),
)
