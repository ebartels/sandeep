from django.contrib import admin
from django.contrib.admin.sites import NotRegistered
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

from ado.adoadmin.sites import AdminApp, hide_admin_apps


# Custom admin app configuration
class AuthAdminApp(AdminApp):
    app = 'auth'
    verbose_name = 'User Management'
    # models = ['User', 'Group']
    models = ['User']


class MediaAdminApp(AdminApp):
    app = 'media'
    verbose_name = 'Media Library'
    models = ['image', 'file', 'video']


class PortfolioAdminApp(AdminApp):
    app = 'portfolio'
    # models = ['Collection']


class ContentAdminApp(AdminApp):
    label = 'content'
    apps = ['local', 'content']
    # models = ['Page']


# These apps will be hidden from the admin index pages.
hide_admin_apps(['sites', 'menus', 'taggit'])


# Customize auth.User admin
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name',
                    'group_col', 'is_staff', 'is_superuser', 'is_active',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'user_permissions'),
                            'classes': ('collapse-closed',)
                            }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined'),
                                'classes': ('collapse-closed',)}),
        (_('Groups'), {'fields': ('groups',)}),
    )

    def group_col(self, obj):
        return ', '.join([g.name for g in obj.groups.all()])
    group_col.short_description = 'Groups'


try:
    admin.site.unregister(User)
except NotRegistered:
    pass

admin.site.register(User, CustomUserAdmin)
