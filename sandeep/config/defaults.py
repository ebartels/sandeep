"""
Default settings for sandeep
"""
import ado
import environ

DEBUG = False

ADMINS = (
    ('Eric Bartels', 'ebartels@gmail.com'),
)
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = '<no-reply@sandeepmukherjee.net>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_SUBJECT_PREFIX = '[sandeepmukherjee.net]'

# project root paths
root = environ.Path(__file__) - 2
cms_root = environ.Path(ado.__file__) - 1

BASE_DIR = BASE_PATH = root()
CMS_DIR = cms_root()

SITE_ID = 1

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Los_Angeles'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Dynamic Media
MEDIA_ROOT = root('../../htdocs/media')
MEDIA_URL = '/m/'

# Static Media
STATIC_ROOT = root('../../htdocs/static')
STATIC_URL = '/s/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    root('../static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    # "ado.menus.context_processors.menuitems",
    # "ado.menus.context_processors.root_menuitems",
    # "ado.menus.context_processors.current_menuitem",
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'ado.utils.middleware.AJAXSimpleExceptionResponse',
)

ROOT_URLCONF = 'sandeep.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'sandeep.wsgi.application'

# Headers used by http proxy middleware
PROXY_HOST_HEADER = 'HTTP_X_FORWARDED_FOR'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')

TEMPLATE_DIRS = (
    root('templates'),
    cms_root('templates'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ado.adoadmin',
    'django.contrib.admin',
    #'django.contrib.admin.apps.SimpleAdminConfig',
    # 'django.contrib.admindocs',
    #'django.contrib.gis',
    #'django.contrib.webdesign',
    #'django.contrib.humanize',
    'keyedcache',
    'livesettings',
    'mptt',
    'sorl.thumbnail',
    'taggit',
    'taggit_templatetags2',
    'webpack_loader',
    'rest_framework',

    'ado.utils',
    'ado.media',
    'ado.menus',
    'ado.apps.content',

    'sandeep.core',
    'sandeep.local',
    'sandeep.portfolio',
    'sandeep.exhibitions',
)

# We only set this explicitly to avoid annoying django warning
# This is the default test runner, and can be removed once using Django 1.8
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Django logging config
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'regular': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'ado': {
            'handlers': ['null'],
            'level': 'WARN',
            'propagate': True,
        },
        'py.warnings': {
            'handlers': ['console'],
        }
    }
}

# Use cooke storage & fall back to session for larger messages
MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

# Default File Storage
DEFAULT_FILE_STORAGE = 'ado.utils.storage.overwrite.OverwriteFileSystemStorage'
FILE_UPLOAD_PERMISSIONS = 0664

# Auth & Profiles
AUTHENTICATION_BACKENDS = (
    # uncomment to use Email as login
    #'ado.utils.auth.backends.EmailAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
)

#LOGIN_URL = '/account/login/'
#LOGIN_REDIRECT_URL = '/'

# Thumbnail settings
THUMBNAIL_PREFIX = 'thumbcache/'
THUMBNAIL_BACKEND = 'ado.media.thumbnail.AutoFormatBackend'
THUMBNAIL_ENGINE = 'ado.media.thumbnail.PILEngine'
THUMBNAIL_UPSCALE = False
THUMBNAIL_DUMMY_SOURCE = 'https://placehold.it/%(width)sx%(height)s/F0EFED/'

# Predefined image to use for creating scaled images
SCALED_IMAGE_SIZES = (
    ('thumb', '200x200'),
    ('small', '200x400'),
    ('medium', '880x800'),
    ('large', '1900x1200'),
)

REST_FRAMEWORK = {
    # 'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',)
}

REST_FRAMEWORK_EXTENSIONS = {
    'DEFAULT_CACHE_ERRORS': False
}

# Path to admin config
ADO_ADMIN_CONFIG = 'sandeep.config.admin'

# Widget to use as the VisualEditor widget
VISUAL_EDITOR = 'ado.utils.forms.widgets.CKTextEditor'

USE_MENU_INDEX_PAGES = False

# Admin editor
CK_EDITOR_CONFIG = {
    'height': '400px',
    'toolbar': [
        ['Bold', 'Italic', 'Strike', '-', 'BulletedList',
         'NumberedList', 'Outdent', 'Indent', 'Blockquote'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
        ['Format', 'FontSize', 'Styles'],
        ['Link', 'Unlink', 'Image', 'Table'],
        ['PasteFromWord', 'RemoveFormat', '-', 'Maximize', '-', 'Source']
    ],
    'allowedContent': True,
    'contentsCss': STATIC_URL + 'bundle/editor_content.css',
    'filebrowserImageBrowseUrl': '/admin/media/image/file_browser/',
    'filebrowserBrowseUrl': '/admin/media/link_browser/',
    'stylesCombo_stylesSet': 'admin_styles:' + STATIC_URL + 'assets/js/editor_styles.js',
    'customConfig': STATIC_URL + "assets/js/editor_extra_config.js"
}
