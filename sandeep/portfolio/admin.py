from django.contrib import admin
from django import forms, http
from django.template import Template, Context

from ado.utils.forms.widgets import VisualEditor
from ado.media.admin.options import RelatedImagesInline

from .models import Collection, CollectionCategory


class CollectionImagesInline(RelatedImagesInline):
    verbose_name_plural = 'Images'
    verbose_name = 'Image'


class CollectionAdmin(admin.ModelAdmin):
    model = Collection
    list_filter = ('published', 'category',)
    list_display = ('image_column', 'title', 'category', 'sort', 'published')
    exclude = ('text', 'date')
    inlines = [CollectionImagesInline]
    save_on_top = True

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'title':
            kwargs.pop('request')
            kwargs['widget'] = forms.TextInput(attrs={'size': '60'})
            return db_field.formfield(**kwargs)

        RICH_FIELDS = ('text',)
        if db_field.name in RICH_FIELDS:
            kwargs.pop('request')
            kwargs['widget'] = VisualEditor(attrs={'rows': '10'})
            return db_field.formfield(**kwargs)

        formfield = super(CollectionAdmin, self).formfield_for_dbfield(
                                                    db_field, **kwargs)
        return formfield

    def image_column(self, obj):
        t = Template("""{% load thumbnail %}
        {% if count %}
        {% thumbnail img.filename "60x60" crop="center" as thumb %}
        <img src="{{thumb.url}}" alt="{{img.filename}}" /><br/>
        {% endthumbnail %}
        {% endif %}
        {{count}} image{{count|pluralize}}
        """)
        count = obj.images.count()
        if count:
            img = obj.images.all()[:1][0]
        else:
            img = None
        return t.render(Context({
            'img': img,
            'count': count
        }))
    image_column.allow_tags = True
    image_column.short_description = 'Images'


class CollectionCategoryAdmin(admin.ModelAdmin):
    model = CollectionCategory
    save_on_top = True

    def get_urls(self):
        from django.conf.urls import patterns, url
        urls = super(CollectionCategoryAdmin, self).get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name
        myurls = patterns('',
            url(r'^sort/$',
                self.admin_site.admin_view(self.sort_view),
                name='%s_%s_sort' % info),
        )
        return myurls + urls

    def sort_view(self, request):
        cats = request.POST.getlist('category')
        for sort, cat_id in enumerate(cats):
            category = CollectionCategory.objects.get(id=cat_id)
            category.sort = sort
            category.save()
        return http.HttpResponse("ok")


admin.site.register(Collection, CollectionAdmin)
admin.site.register(CollectionCategory, CollectionCategoryAdmin)
