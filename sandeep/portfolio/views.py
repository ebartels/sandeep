from django.views.generic import DetailView

from .models import Collection, CollectionCategory


class CategoryDetailView(DetailView):
    queryset = CollectionCategory.objects.filter(published=True)
    template_name = 'index.html'

category_detail = CategoryDetailView.as_view()


class CollectionDetailView(DetailView):
    queryset = Collection.objects.filter(published=True)
    template_name = 'index.html'

collection_detail = CollectionDetailView.as_view()
