from django.db import models

from positions import PositionField
from ado.media.models import RelatedImagesField
from ado.utils.fields import AutoSlugField
from ado import menus


class CollectionCategory(models.Model):
    name = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='name',
            help_text='Unique text identifier used in urls.')
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    sort = PositionField('Position',
            help_text='Lower numbers will appear first on site.')

    class Meta:
        ordering = ('sort', 'id')
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('portfolio:category-detail', [self.slug])


class Collection(models.Model):
    """
    A Collection of images
    """
    title = models.CharField(max_length=200)
    slug = AutoSlugField(max_length=200, unique=True, editable=False,
            prepopulate_from='title',
            help_text='Unique text identifier used in urls.')
    date = models.DateField(blank=True, null=True)
    published = models.BooleanField(default=True,
            help_text='Whether to publish on the site.')
    text = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    sort = PositionField('Position', collection='category',
            help_text='Lower numbers appear first on site.')

    category = models.ForeignKey('CollectionCategory',
                                 null=True, blank=True,
                                 related_name='collections')

    images = RelatedImagesField()

    class Meta:
        ordering = ('category', 'sort')

    def __unicode__(self):
        return u'{0} > {1}'.format(self.category, self.title)

    def has_media(self):
        return self.images.exists()


menus.register_model(CollectionCategory)
