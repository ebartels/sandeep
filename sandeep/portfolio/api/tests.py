from __future__ import unicode_literals

import factory
import factory.django
from django_webtest import WebTest
from django.core.urlresolvers import reverse


class CollectionCategoryFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Category {0}'.format(n))
    slug = factory.Sequence(lambda n: 'category-{0}'.format(n))

    class Meta:
        model = 'portfolio.CollectionCategory'


class CollectionFactory(factory.DjangoModelFactory):
    title = factory.Sequence(lambda n: 'Collection {0}'.format(n))
    slug = factory.Sequence(lambda n: 'collection-{0}'.format(n))

    class Meta:
        model = 'portfolio.Collection'


class CategoryDetailViewTest(WebTest):

    def setUp(self):
        self.category = CollectionCategoryFactory(name='Works',
                                                  slug='works')

        self.collections = [
            CollectionFactory(category=self.category),
            CollectionFactory(category=self.category),
        ]

    def test_get(self):
        url = reverse('api:portfolio:category-detail', args=['works'])
        resp = self.app.get(url, status=200)
        resp_keys = set(resp.json.keys())
        self.assertSetEqual(
            resp_keys,
            set([
                'name',
                'slug',
                'collections',
            ]))
        self.assertEqual(len(resp.json['collections']), 2)

    def test_get_unpublished(self):
        self.category.published = False
        self.category.save()
        url = reverse('api:portfolio:category-detail', args=['works'])
        self.app.get(url, status=404)

    def test_get_unpublished_collection(self):
        CollectionFactory(category=self.category, published=False)
        url = reverse('api:portfolio:category-detail', args=['works'])
        resp = self.app.get(url, status=200)
        self.assertEqual(len(resp.json['collections']), 2)


class CollectionListApiViewTest(WebTest):

    def setUp(self):
        self.category = CollectionCategoryFactory()

        self.collections = [
            CollectionFactory(category=self.category),
            CollectionFactory(category=self.category),
            CollectionFactory(category=CollectionCategoryFactory()),
        ]

        self.url = reverse('api:portfolio:collection-list')

    def test_get(self):
        resp = self.app.get(self.url, status=200)
        self.assertEqual(len(resp.json), 3)
        resp_keys = set(resp.json[0].keys())
        self.assertSetEqual(
            resp_keys,
            set([
                'title',
                'slug',
                'images',
            ]))

    def test_get_for_category(self):
        url = '{0}?category={1}'.format(self.url, self.category.slug)
        resp = self.app.get(url, status=200)
        self.assertEqual(len(resp.json), 2)

    def test_get_unpublished(self):
        CollectionFactory(category=self.category, published=False)
        resp = self.app.get(self.url, status=200)
        self.assertEqual(len(resp.json), 3)
