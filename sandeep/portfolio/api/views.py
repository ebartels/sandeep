from __future__ import unicode_literals

from rest_framework import generics
from sandeep.core.api.views import (ListCacheResponseMixin,
                                    RetrieveCacheResponseMixin)

from ..models import Collection, CollectionCategory
from . import serializers


class CollectionListView(ListCacheResponseMixin, generics.ListAPIView):
    serializer_class = serializers.CollectionSerializer

    def get_queryset(self):
        qs = (Collection.objects
                .filter(published=True)
                .order_by('category__sort', 'sort'))

        if 'category' in self.request.GET:
            qs = qs.filter(category__slug=self.request.GET['category'])

        return qs


class CategoryDetailView(RetrieveCacheResponseMixin, generics.RetrieveAPIView):
    serializer_class = serializers.CollectionCategorySerializer
    queryset = CollectionCategory.objects.filter(published=True)
    lookup_field = 'slug'
