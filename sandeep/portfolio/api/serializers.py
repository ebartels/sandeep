from __future__ import unicode_literals

from rest_framework import serializers
from sandeep.portfolio.models import Collection, CollectionCategory
from sandeep.core.api.serializers import ImageSerializer


class CollectionSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True)

    class Meta:
        model = Collection
        fields = [
            'title',
            'slug',
            'images',
        ]


class CollectionCategorySerializer(serializers.ModelSerializer):
    collections = serializers.SerializerMethodField()

    class Meta:
        model = CollectionCategory
        fields = [
            'name',
            'slug',
            'collections',
        ]

    def get_collections(self, obj):
        collections = (obj.collections
                       .filter(published=True)
                       .select_related()
                       .order_by('sort'))
        ser = CollectionSerializer(collections, many=True)
        return ser.data
