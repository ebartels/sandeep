from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^category/(?P<slug>[\w-]+)/?$',
        views.CategoryDetailView.as_view(),
        name='category-detail'),

    url(r'^collection/?$',
        views.CollectionListView.as_view(),
        name='collection-list'),
]
