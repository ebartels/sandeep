# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ado.media.fields.related
import ado.utils.fields
import positions.fields


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('slug', ado.utils.fields.AutoSlugField(editable=False, prepopulate_from=b'title', max_length=200, help_text=b'Unique text identifier used in urls.', unique=True, slugify=ado.utils.fields.slugify)),
                ('date', models.DateField(null=True, blank=True)),
                ('published', models.BooleanField(default=True, help_text=b'Whether to publish on the site.')),
                ('text', models.TextField(blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('sort', positions.fields.PositionField(default=-1, help_text=b'Lower numbers appear first on site.', verbose_name=b'Position')),
            ],
            options={
                'ordering': ('category', 'sort'),
            },
        ),
        migrations.CreateModel(
            name='CollectionCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', ado.utils.fields.AutoSlugField(editable=False, prepopulate_from=b'name', max_length=200, help_text=b'Unique text identifier used in urls.', unique=True, slugify=ado.utils.fields.slugify)),
                ('published', models.BooleanField(default=True, help_text=b'Whether to publish on the site.')),
                ('sort', positions.fields.PositionField(default=-1, help_text=b'Lower numbers will appear first on site.', verbose_name=b'Position')),
            ],
            options={
                'ordering': ('sort', 'id'),
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.AddField(
            model_name='collection',
            name='category',
            field=models.ForeignKey(related_name='collections', blank=True, to='portfolio.CollectionCategory', null=True),
        ),
        migrations.AddField(
            model_name='collection',
            name='images',
            field=ado.media.fields.related.RelatedImagesField(through=b'media.ImageRelation', verbose_name=b'Images', to=b'media.Image'),
        ),
    ]
