from django.conf.urls import url, patterns

urlpatterns = patterns('sandeep.portfolio.views',
    url(r'^(?P<slug>[\w-]+)/$', 'category_detail',
        name="category-detail"),

    url(r'^(?P<slug>[\w-]+)/.+/$', 'category_detail'),

    url(r'^collection/(?P<slug>[\w-]+)/$', 'collection_detail',
        name='collection-detail'),
)
