from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib import admin

import sandeep.config.livesettings_config  # noqa
from ado import menus

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),

    url(r'^pages/', include('sandeep.pages.urls')),
    url(r'^portfolio/', include('sandeep.portfolio.urls', namespace='portfolio')),
    url(r'^exhibitions/', include('sandeep.exhibitions.urls', namespace='exhibitions')),

    # Old urls: remove me
    # url(r'^index/', include('ado.menus.urls')),
    # url(r'^pages/', include('ado.apps.content.urls.pages')),
    # url(r'^portfolio/', include('sandeep.portfolio.urls')),
    # url(r'^exhibitions/', include('sandeep.exhibitions.urls')),

    # ado
    url(r'^media/', include('ado.media.urls')),

    # Admin:
    url(r'^admin/settings/', include('livesettings.urls')),
    url(r'^admin/media/', include('ado.media.admin.urls')),
    url(r'^admin/', include('ado.adoadmin.urls')),
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

# API Urls
urlpatterns += [
    url(r'^api/', include('sandeep.core.api.urls', namespace="api")),
]

# MEDIA For Development Server
urlpatterns += static(settings.MEDIA_URL,
                      document_root=settings.MEDIA_ROOT,
                      show_indexes=True)

menus.register_url('Exhibitions', name='exhibitions:list')
