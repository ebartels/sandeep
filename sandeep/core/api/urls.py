from django.conf.urls import url, include


urlpatterns = [
    url('^portfolio/', include('sandeep.portfolio.api.urls', namespace='portfolio')),
    url('^exhibitions/', include('sandeep.exhibitions.api.urls', namespace='exhibitions')),
    url('^pages/', include('sandeep.pages.api.urls', namespace='pages')),
    url('^menu/', 'sandeep.core.api.menus.views.menu', name='menu'),
    url('^local/', include('sandeep.local.api.urls', namespace='local')),
]
