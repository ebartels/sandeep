from __future__ import unicode_literals

from sorl import thumbnail
from rest_framework import serializers
from django.conf import settings

from ado.media.models import Image, Video, File


IMAGE_SIZES = getattr(settings, 'SCALED_IMAGE_SIZES', tuple())


class ImageSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    filename = serializers.SerializerMethodField()
    resized = serializers.SerializerMethodField()
    ratio = serializers.SerializerMethodField()
    is_portrait = serializers.SerializerMethodField()

    class Meta:
        model = Image
        fields = ('id',
                  'type',
                  'uuid',
                  'url',
                  'filename',
                  'title',
                  'caption',
                  'alt_text',
                  'width',
                  'height',
                  'ratio',
                  'is_portrait',
                  'resized',
                  )

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super(ImageSerializer, self).__init__(self, *args, **kwargs)

    def get_url(self, obj):
        return obj.filename.url

    def get_filename(self, obj):
        return obj.filename.name

    def get_ratio(self, obj):
        return float(obj.width) / obj.height

    def get_is_portrait(self, obj):
        return self.get_ratio(obj) < 1

    def get_resized(self, obj):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in self.sizes:
            thumb = thumbnail.get_thumbnail(obj.filename, size)
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
            }
        return images


class VideoSerializer(serializers.ModelSerializer):
    info = serializers.SerializerMethodField()
    resized = serializers.SerializerMethodField()
    ratio = serializers.SerializerMethodField()
    is_portrait = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = ('id',
                  'type',
                  'uuid',
                  'url',
                  'title',
                  'caption',
                  'image',
                  'info',
                  'ratio',
                  'is_portrait',
                  'resized',
                 )

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super(VideoSerializer, self).__init__(self, *args, **kwargs)

    def get_info(self, obj):
        return obj.info

    def get_ratio(self, obj):
        resized = self.get_resized(obj)
        item = resized.values()[0]
        return float(item['width']) / float(item['height'])

    def get_is_portrait(self, obj):
        return self.get_ratio(obj) < 1

    def get_resized(self, obj):
        images = {}
        # for key, size in (('medium', '420x9000'),):
        for key, size in self.sizes:
            thumb = thumbnail.get_thumbnail(obj.image, size)
            images[key] = {
                'url': thumb.url,
                'width': thumb.width,
                'height': thumb.height,
            }
        return images


class FileSerializer(serializers.ModelSerializer):

    class Meta:
        model = File
        fields = ('id',
                  'type',
                  'uuid',
                  'title',
                  'caption',
                  'filename',
                  )


class MediaSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        self.sizes = kwargs.pop('sizes', IMAGE_SIZES)
        super(MediaSerializer, self).__init__(self, *args, **kwargs)

    def to_representation(self, instance):
        if instance.type == 'image':
            return (ImageSerializer(sizes=self.sizes, context=self.context)
                        .to_representation(instance))
        elif instance.type == 'video':
            return (VideoSerializer(sizes=self.sizes, context=self.context)
                        .to_representation(instance))
        elif instance.type == 'file':
            return (FileSerializer(context=self.context)
                        .to_representation(instance))
