from django.apps import AppConfig


class CoreAppConfig(AppConfig):
    name = 'sandeep.core'

    def ready(self):
        from sandeep.core import listeners  # noqa
