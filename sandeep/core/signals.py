from django import dispatch

post_bulk_update = dispatch.Signal()
post_bulk_delete = dispatch.Signal()
