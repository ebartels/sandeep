from django.contrib import admin
from django.template import Template, Context

from ado.adoadmin.admin import ADOModelAdminMixin
from sandeep.local.models import HomepageImage


class HomepageImageAdmin(ADOModelAdminMixin, admin.ModelAdmin):
    list_display = ('image_column', 'active',)
    ordering = ('-active',)
    verbose_image_fk_fields = ['image']

    def image_column(self, obj):
        t = Template("""{% load thumbnail %}
            {% thumbnail obj.image.filename "120x120" as thumb %}
            <img src="{{thumb.url}}" alt="" />
            {% endthumbnail %}
            """)
        return t.render(Context({'obj': obj}))
    image_column.allow_tags = True
    image_column.short_description = 'Image'


admin.site.register(HomepageImage, HomepageImageAdmin)
