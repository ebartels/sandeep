from __future__ import unicode_literals

import factory
import factory.django
from django_webtest import WebTest
from django.core.urlresolvers import reverse
from ado.media.tests.tests import ImageFactory


class HomepageImageFactory(factory.DjangoModelFactory):
    image = factory.SubFactory(ImageFactory)
    active = True

    class Meta:
        model = 'local.HomepageImage'


class HomepageImageListApiViewTest(WebTest):

    def setUp(self):
        self.homepage_images = [
            HomepageImageFactory(),
            HomepageImageFactory(),
            HomepageImageFactory(),
        ]

        self.url = reverse('api:local:homepageimage-list')

    def test_get(self):
        resp = self.app.get(self.url, status=200)
        self.assertEqual(len(resp.json), 3)
        resp_keys = set(resp.json[0].keys())
        self.assertSetEqual(
            resp_keys,
            set([
                'image',
            ]))

    def test_get_with_inactive(self):
        HomepageImageFactory(active=False)
        resp = self.app.get(self.url, status=200)
        self.assertEqual(len(resp.json), 3)
