from __future__ import unicode_literals

from rest_framework import serializers
from sandeep.core.api.serializers import ImageSerializer
from ..models import HomepageImage


class HomepageImageSerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = HomepageImage
        fields = ['image']
