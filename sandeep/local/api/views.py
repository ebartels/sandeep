from __future__ import unicode_literals

from rest_framework import generics
from sandeep.core.api.views import ListCacheResponseMixin

from ..models import HomepageImage
from . import serializers


class HomepageImageListView(ListCacheResponseMixin, generics.ListAPIView):
    serializer_class = serializers.HomepageImageSerializer

    def get_queryset(self):
        return HomepageImage.objects.filter(active=True)
