from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^homepageimage/?$',
        views.HomepageImageListView.as_view(),
        name='homepageimage-list'),
]
