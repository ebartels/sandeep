from django.db import models


class HomepageImage(models.Model):
    image = models.ForeignKey('media.Image')
    active = models.BooleanField(default=True,
            help_text='Set as the active homepage image.')

    def __unicode__(self):
        return self.image.title
