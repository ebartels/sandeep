from django.template.response import TemplateResponse
from sandeep.local.models import HomepageImage


def home(request):
    try:
        image = HomepageImage.objects.get(active=True)
    except HomepageImage.DoesNotExist:
        image = None
    return TemplateResponse(request, 'index.html', {
        'image': image,
    })
