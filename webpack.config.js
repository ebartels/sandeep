// Get environment (development|production|hot)
const env = process.env.NODE_ENV || 'development'

// require config
module.exports = require(`./config/webpack.${env}.config.js`)
