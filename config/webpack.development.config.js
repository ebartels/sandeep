/**
 * Webpack config for default dev builds
 */

const WebpackCleanupPlugin = require('webpack-cleanup-plugin')
const config = require('./webpack.base.config.js')

config.devtool = 'inline-cheap-module-eval-source-map'

config.plugins = config.plugins.concat([
  new WebpackCleanupPlugin({
    exclude: ['webpack-stats.json'],
  }),
])

module.exports = config
