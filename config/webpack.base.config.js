/**
 * Base webpack config
 */
const path = require('path')
const webpack = require('webpack')
const BundleTracker = require('webpack-bundle-tracker')
const autoprefixer = require('autoprefixer')

module.exports = {
  context: path.resolve(path.join(__dirname, '..')),

  entry: {
    main: [
      'babel-polyfill',
      'whatwg-fetch',
      './app/index',
    ],
  },

  output: {
    path: path.resolve('./static/bundle'),
    filename: '[name]-[hash].js',
    publicPath: '/s/bundle/',
  },

  resolve: {
    root: path.resolve('./app'),
    extensions: ['', '.js', '.jsx', '.json'],
  },

  module: {

    // Loaders for different file types
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel?cacheDirectory',
      },
      {
        test: /\.json$/,
        loader: 'json',
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ['style', 'css?sourceMap', 'postcss', 'sass?sourceMap'],
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        loaders: ['style', 'css?sourceMap', 'postcss'],
      },
      {
        test: /\.(png|gif|jpg|jpeg|svg)?$/,
        loader: 'url',
      },
      {
        test: /\.(ttf|eot|svg|woff|woff2?)(\?\S*)?$/,
        loader: 'url',
      },
    ],

    // These deps do not use 'require' so can skip parsing and speed things up
    noParse: [
      // path.resolve('./node_modules/jquery'),
    ],
  },

  plugins: [
    // Outputs stats on generated files (used by django-webpack-loader)
    new BundleTracker({ filename: './static/bundle/webpack-stats.json' }),

    // Do not import moment locales (en default only)
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],

  // scss import paths
  sassLoader: {
    includePaths: [
      './app/styles/',
      './app/styles/foundation/scss',
    ],
  },

  postcss: [
    autoprefixer(),
  ],
}
