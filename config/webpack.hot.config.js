/**
 * Webpack config for running webpack-dev-server and  react-hmre
 */
const config = require('./webpack.base.config.js')

// Set up dev server entry point
Object.keys(config.entry).forEach(k => {
  config.entry[k] = [].concat([
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3000',
  ], config.entry[k])
})

// Set publicPath for use with dev-server
config.output.publicPath = 'http://localhost:3000/bundles/'

// source maps
config.devtool = 'inline-cheap-eval-source-map'
// The following doesn't seem to work with hot reload 3
// config.devtool = 'inline-cheap-module-eval-source-map'

module.exports = config
