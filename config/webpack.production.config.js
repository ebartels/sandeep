/**
 * Webpack config for production builds
 */
const webpack = require('webpack')
const WebpackCleanupPlugin = require('webpack-cleanup-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const config = require('./webpack.base.config.js')

config.plugins = config.plugins.concat([

  // removes a lot of debugging code in React
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
  }),

  // Extracts styles in a separate bundle
  new ExtractTextPlugin('styles-[hash].css', {
    allChunks: true,
  }),

  // Keep hashes consistent between compilations
  new webpack.optimize.OccurenceOrderPlugin(),

  // Remove duplicated code
  new webpack.optimize.DedupePlugin(),

  // minify js code
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: false,
    output: {
      comments: false,
    },
    compress: {
      warnings: false,
    },
  }),

  // cleanup stale build files
  new WebpackCleanupPlugin({
    exclude: ['webpack-stats.json'],
  }),
])

// Extract css into separate bundle (css/scss)
config.module.loaders[2].loader = ExtractTextPlugin.extract('style', 'css?minimize!postcss!sass')
config.module.loaders[3].loader = ExtractTextPlugin.extract('style', 'css?minimize!postcss')

// Files loader for fonts
config.module.loaders[5].loader = 'file'

module.exports = config
