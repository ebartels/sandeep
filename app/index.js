import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import App from './App'

const rootEl = document.getElementById('root')

renderWithHotReload(App)

if (module.hot) {
  module.hot.accept('./App', () => {
    // If you use Webpack 2 in ES modules mode, you can
    // use <App /> here rather than require() a <NextApp />.
    renderWithHotReload(require('./App').default) // eslint-disable-line global-require
  })
}

// AppContainer is a wrapper element providing hot reloading through
// react-hot-loader. It is a noop when NODE_ENV === 'production'
function renderWithHotReload(RootElement) {
  ReactDOM.render(
    <AppContainer>
      <RootElement />
    </AppContainer>,
    rootEl
  )
}
