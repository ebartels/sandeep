/* @flow weak */
import React from 'react'
import { default as cx } from 'classnames'
import queryString from 'query-string'
import MediaItemImage from 'ui/components/MediaItemImage'
import MediaItemVideo from 'ui/components/MediaItemVideo'

type Props = {
  item: Object,
  placement: number,
  shouldLoad?: boolean,
  onClick: (e: Event) => void,
}

type State = {
  videoPlaying: boolean,
}

export class MediaItem extends React.Component {
  props: Props

  static defaultProps = {
    shouldLoad: true,
  }

  state: State = {
    videoPlaying: false,
  }

  componentWillMount() {
    const query = queryString.parse(location.search)
    this.setState({
      videoPlaying: this.props.placement === 0 && query.play !== undefined,
    })
  }

  componentWillReceiveProps(newProps) {
    if (newProps.placement !== 0) {
      this.setState({ videoPlaying: false })
    }
  }

  get isActive() {
    return this.props.placement === 0
  }

  get isNext() {
    return this.props.placement === 1
  }

  get isPrev() {
    return this.props.placement === -1
  }

  get style() {
    const { placement } = this.props
    const visible = placement >= -1 && placement <= 1
    const y = this.props.placement * 100

    return {
      transform: visible ? `translate3d(0, ${y}%, 0)` : '',
      zIndex: this.isActive ? 2 : 1,
      display: visible ? '' : 'none',
    }
  }

  get className() {
    return cx('media-view-item', {
      active: this.isActive,
      next: this.isNext,
      prev: this.isPrev,
    })
  }

  handlePlayClick = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({ videoPlaying: true })
  }

  renderMediaItem() {
    const { item } = this.props
    switch (item.type) {
      case 'video':
        return (
          <MediaItemVideo
            item={item}
            onPlayClick={this.handlePlayClick}
            isPlaying={this.state.videoPlaying} />
        )
      default:
        return <MediaItemImage item={item} />
    }
  }

  render() {
    const { item, ...props } = this.props
    return (
      <div
        className={cx(this.className, `type-${item.type}`)}
        style={this.style}
        onClick={props.onClick}>

        <div className="media-content">
          {props.shouldLoad ? this.renderMediaItem() : null}
        </div>
      </div>
    )
  }
}

export default MediaItem
