/* @flow weak */
/* eslint react/no-unused-prop-types: 0 */
import React from 'react'
import { default as cx } from 'classnames'
import throttle from 'lodash.throttle'
import Hammer from 'react-hammerjs'
import hammer from 'hammerjs'
import WheelHandler from 'ui/components/WheelHandler'
import Markup from 'ui/components/Markup'
import MediaItem from 'ui/components/MediaItem'
import Icon from 'ui/components/Icon'

type Props = {
  items: Object[],
  activeIndex: number,
  getPlacement: (i: number) => number,
  className?: string,
  getShouldLoad: (i: number) => boolean,
  onClick: (e: Event) => void,
  onPrevClick: (e: Event) => void,
  onNextClick: (e: Event) => void,
  onCursorMove: (e: Event) => void,
  onWheel: (e: Event) => void,
  onSwipe: (e: Event) => void,
}

export const MediaView = (props: Props) => (
  <WheelHandler onWheel={props.onWheel}>
    <Hammer onSwipe={props.onSwipe} direction="DIRECTION_ALL">
      <div className={props.className}>
        <div
          className="media-view-slider"
          onMouseMove={props.onCursorMove}>
          {props.items.map((item, i) => (
            <MediaItem
              key={i}
              item={item}
              placement={props.getPlacement(i)}
              shouldLoad={props.getShouldLoad(i)}
              onClick={props.onClick}
            />
          ))}
        </div>

        <div className="media-caption">
          <Markup removeEmptyTags>{props.items[props.activeIndex || 0].caption}</Markup>
        </div>

        <button className="nav-prev" onClick={props.onPrevClick}><Icon name="angle-up" /></button>
        <button className="nav-next" onClick={props.onNextClick}><Icon name="angle-down" /></button>
        <span className="nav-count">{props.activeIndex + 1} / {props.items.length}</span>
      </div>
    </Hammer>
  </WheelHandler>
)

MediaView.defaultProps = {
  getShouldLoad: () => true,
}


type ContainerProps = {
  items: Object[],
  activeId: string,
  onShowItem: (i: number) => void,
}

type ContainerState = {
  cursorOrientation: 'down' | 'up',
}

export class MediaViewContainer extends React.Component {
  props: ContainerProps

  static defaultProps = {
    onShowItem: () => {},
  }

  state: ContainerState = {
    cursorOrientation: 'down',
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown)
  }

  getIndex(i) {
    const n = this.items.length
    return ((i % n) + n) % n
  }

  get items() {
    return this.props.items
  }

  get activeId() {
    if (this.props.activeId) {
      return parseInt(this.props.activeId, 10)
    }
    return this.items[0].id
  }

  get activeIndex() {
    return this.items.findIndex(i => i.id === this.activeId)
  }

  get nextIndex() {
    return this.getIndex(this.activeIndex + 1)
  }

  get prevIndex() {
    return this.getIndex(this.activeIndex - 1)
  }

  get nextId() {
    return this.items[this.nextIndex].id
  }

  get prevId() {
    return this.items[this.prevIndex].id
  }

  goNext() {
    if (this.props.items.length < 2) return
    this.props.onShowItem(this.nextId)
  }

  goPrev() {
    if (this.props.items.length < 2) return
    this.props.onShowItem(this.prevId)
  }

  getPlacement = (i) => {
    if (this.activeIndex === i) {
      return 0
    }
    let placement = i - this.activeIndex
    if (placement >= this.items.length - 1) {
      placement -= this.items.length
    }
    else if (placement <= (-1 * this.items.length) + 1) {
      placement += this.items.length
    }
    return placement
  }

  getShouldLoad = (i) => {
    const placement = this.getPlacement(i)
    return placement >= -1 && placement <= 2
  }

  handleKeyDown = throttle(e => {
    switch (e.which) {
      case 40:
      case 34:
        this.goNext()
        break
      case 38:
      case 33:
        this.goPrev()
        break
      default:
        break
    }
  }, 300, { leading: true, trailing: true })

  handleClick = e => {
    if (this.state.cursorOrientation === 'up') {
      this.goPrev()
    }
    else {
      this.goNext()
    }
  }

  handlePrevClick = e => {
    this.goPrev()
  }

  handleNextClick = e => {
    this.goNext()
  }

  handleCursorMove = e => {
    const wH = window.innerHeight
    const orientation = e.pageY >= (wH * 0.3) ? 'down' : 'up'
    if (orientation !== this.state.cursorOrientation) {
      this.setState({
        cursorOrientation: orientation,
      })
    }
  }

  handleMouseWheel = e => {
    if (e.direction === 'down') {
      this.goNext()
    }
    else {
      this.goPrev()
    }
  }

  handleSwipe = e => {
    if (e.direction === hammer.DIRECTION_UP) {
      this.goNext()
    }
    else if (e.direction === hammer.DIRECTION_DOWN) {
      this.goPrev()
    }
  }

  render() {
    return (
      <MediaView
        items={this.items}
        activeIndex={this.activeIndex}
        getPlacement={this.getPlacement}
        getShouldLoad={this.getShouldLoad}
        onClick={this.handleClick}
        onPrevClick={this.handlePrevClick}
        onNextClick={this.handleNextClick}
        onCursorMove={this.handleCursorMove}
        onWheel={this.handleMouseWheel}
        onSwipe={this.handleSwipe}
        className={cx(
          'media-view',
          {
            'single-item': this.items.length < 2,
            'cursor-up': this.state.cursorOrientation === 'up',
            'cursor-down': this.state.cursorOrientation === 'down',
          }
        )}
      />
    )
  }
}

export default MediaViewContainer
