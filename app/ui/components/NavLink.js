/* @flow */
/* eslint jsx-a11y/anchor-has-content: 0 */
import React from 'react'
import { Link } from 'react-router'

type Props = { }

export const NavLink = (props: Props) => (
  <Link {...props} activeClassName="active" />
)

export default NavLink
