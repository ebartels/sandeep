/* @flow weak */
import React from 'react'

type Props = {
  className: string,
  children?: React$Element<*>,
}

export class BodyClass extends React.Component {
  props: Props

  static get body() {
    return document.body
  }

  static addClass(cls) {
    BodyClass.body.classList.add(cls)
  }

  static removeClass(cls) {
    BodyClass.body.classList.remove(cls)
  }


  componentDidMount() {
    BodyClass.addClass(this.props.className)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.className !== this.props.className) {
      BodyClass.removeClass(prevProps.className)
      BodyClass.addClass(this.props.className)
    }
  }

  componentWillUnmount() {
    BodyClass.removeClass(this.props.className)
  }

  render() {
    return this.props.children
  }
}

export default BodyClass
