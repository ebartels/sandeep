/* @flow weak */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import Masonry from 'ui/components/Masonry'
import MasonryImage from 'ui/components/MasonryImage'
import { Link } from 'react-router'
import { default as cx } from 'classnames'
import { MOBILE_MAX_WIDTH } from 'core/constants'
import { isMobile } from 'core/dom'

type Props = {
  images: Object[],
  urlBase: string,
  isMobile: boolean,
  onLayoutComplete?: () => {},
}

export const ImageList = (props: Props) => (
  <div className="image-list">
    <Masonry
      minWidth={MOBILE_MAX_WIDTH + 1}
      onLayoutComplete={props.onLayoutComplete}
      options={{
        itemSelector: '.image-item',
        columnWidth: '.image-item',
        percentPosition: true,
        transitionDuration: 0,
      }}>
      {props.images.map((item, i) => (
        <Link
          to={`${props.urlBase}/detail/${item.id}`}
          className={cx('image-item', `type-${item.type}`)}
          key={i}>
          <MasonryImage
            src={item.resized[props.isMobile ? 'medium' : 'small'].url}
            width={item.resized[props.isMobile ? 'medium' : 'small'].width}
            height={item.resized[props.isMobile ? 'medium' : 'small'].height}
            alt={item.alt_text} />
        </Link>
      ))}
    </Masonry>
  </div>
)

type ContainerProps = {
  images: Object[],
  onLayoutComplete?: () => {},
}

export class ImageListContainer extends React.Component {
  props: ContainerProps

  state = {
    isMobile: false,
  }

  checkMobile = () => {
    if (isMobile()) {
      if (!this.state.isMobile) {
        this.setState({ isMobile: true })
      }
    }
    else if (this.state.isMobile) {
      this.setState({ isMobile: false })
    }
  }

  componentWillMount() {
    this.checkMobile()
  }

  componentDidMount() {
    this.checkMobile()
    window.addEventListener('resize', this.checkMobile)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.checkMobile)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  render() {
    const urlBase = window.location.pathname.replace(/\/$/, '')
    return (
      <ImageList
        images={this.props.images}
        urlBase={urlBase}
        isMobile={this.state.isMobile}
        onLayoutComplete={this.props.onLayoutComplete}
      />
    )
  }
}

export default ImageListContainer
