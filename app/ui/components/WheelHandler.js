/* @flow */
/* eslint react/no-find-dom-node: 0 */
import React from 'react'
import ReactDOM from 'react-dom'
import WheelIndicator from 'wheel-indicator'

type Props = {
  children?: React$Element<*>,
  onWheel: (e: Object) => void,
}

export class WheelHandler extends React.Component {
  props: Props
  wheelIndicator: Object

  componentDidMount() {
    const el = ReactDOM.findDOMNode(this)
    this.wheelIndicator = new WheelIndicator({
      elem: el,
      callback: this.handleMouseWheel,
      preventMouse: true,
    })
  }

  componentWillUnmount() {
    this.wheelIndicator.destroy()
  }

  handleMouseWheel = (e: Event) => {
    this.props.onWheel(e)
  }

  render() {
    return this.props.children
  }
}

export default WheelHandler
