/* @flow */
import React from 'react'

type Props = {
  item: Object,
}

export const MediaItemImage = ({ item }: Props) => (
  <img
    src={item.resized.large.url}
    alt={item.alt_text} />
)

export default MediaItemImage
