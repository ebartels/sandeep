/* @flow */
import React from 'react'
import { default as cx } from 'classnames'

type Props = {
  name: string,
}

export const Icon = (props: Props) => (
  <i className={cx('zmdi', `zmdi-${props.name}`)} />
)

export default Icon
