/* @flow */
import React from 'react'
import { default as DocumentTitleComponent } from 'react-document-title'
import { DOCUMENT_TITLE } from 'core/constants'

type Props = {
  title?: string,
  children?: React$Element<*>,
}

export const DocumentTitle = (props: Props) => (
  <DocumentTitleComponent title={mergeTitle(props.title)}>
    { props.children }
  </DocumentTitleComponent>
)

function mergeTitle(title?: string, base?: string = DOCUMENT_TITLE): string {
  if (title && title !== base) {
    return `${title} | ${base}`
  }
  return base
}

export default DocumentTitle
