/* @flow */
import React from 'react'
import Icon from 'ui/components/Icon'
import Video from 'ui/components/Video'

type Props = {
  item: Object,
  onPlayClick?: (e: Event) => void,
  isPlaying?: boolean,
}

export const MediaItemVideo = ({ item, ...props }: Props) => (
  props.isPlaying
  ? <Video data={item.info} />
  : (
    <div className="video-image">
      <img
        src={item.resized.large.url}
        alt={item.alt_text} />
      <button
        className="play-button"
        onClick={props.onPlayClick}><Icon name="play" /></button>
    </div>
  )
)

export default MediaItemVideo
