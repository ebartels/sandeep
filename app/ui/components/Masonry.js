/* @flow weak */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { default as MasonryComponent } from 'react-masonry-component'

type Props = {
  children?: React$Element<*>,
  minWidth?: number,
  onLayoutComplete?: Function,
}

type State = {
  useMasonry: boolean,
}

export class Masonry extends React.Component {
  props: Props

  state: State = {
    useMasonry: false,
  }

  componentWillMount() {
    this.handleResize()
  }

  componentDidMount() {
    this.handleResize()
    window.addEventListener('resize', this.handleResize)
    if (!this.state.useMasonry && this.props.onLayoutComplete) {
      this.props.onLayoutComplete()
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  disableMasonry() {
    this.setState({ useMasonry: false })
  }

  enableMasonry() {
    this.setState({ useMasonry: true })
  }

  handleResize = () => {
    if (this.props.minWidth) {
      const wW = window.innerWidth
      if (wW < this.props.minWidth) {
        this.disableMasonry()
      }
      else {
        this.enableMasonry()
      }
    }
  }

  render() {
    if (!this.state.useMasonry) {
      return <span>{this.props.children}</span>
    }

    const masonryProps = { ...this.props }
    ;['minWidth'].forEach(p => delete masonryProps[p])

    return (
      <div>
        <MasonryComponent
          {...masonryProps}
        />
      </div>
    )
  }
}

export default Masonry
