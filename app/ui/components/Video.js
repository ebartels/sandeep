/* @flow weak */
/* eslint react/no-danger: 0 */
import React from 'react'
import queryString from 'query-string'

type Props = {
  data: Object,
}

type State = {
  maxWidth: number,
  maxHeight: number,
}

export class Video extends React.Component {
  props: Props
  el: Object  // HTMLElement

  state: State = {
    maxWidth: 0,
    maxHeight: 0,
  }

  static videoOptions = {
    youtube: {
      autoplay: 1,
      modestbranding: 1,
      showinfo: 0,
    },
    vimeo: {
      autoplay: 1,
      badge: 0,
      byline: 0,
      portrait: 0,
      title: 0,
    },
  }

  componentDidMount() {
    this.setVideoSrc()
    this.handleResize()
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  setVideoSrc = () => {
    // get video iframe and alter src and set options
    const iframe = this.el.querySelector('iframe')
    const [url, querystring] = iframe.getAttribute('src').split('?')
    const query = queryString.parse(querystring)
    const provider = this.props.data.provider_name.toLowerCase()
    const options = Video.videoOptions[provider]
    if (options) {
      Object.assign(query, options)
    }
    iframe.setAttribute('src', `${url}?${queryString.stringify(query)}`)
  }

  handleResize = () => {
    const parent = this.el.parentNode
    if (parent) {
      const maxWidth = Math.min(parent.offsetWidth, 1900)
      const maxHeight = parent.scrollHeight
      this.setState({ maxWidth, maxHeight })
    }
  }

  get style() {
    const { maxWidth, maxHeight } = this.state
    const ratio = this.props.data.width / this.props.data.height
    let width = maxWidth
    let height = width / ratio

    if (width > maxWidth) {
      width = maxWidth
      height = width / ratio
    }
    if (height > maxHeight) {
      height = maxHeight
      width = height * ratio
    }

    return { width, height }
  }

  render() {
    return (
      <div
        ref={c => { this.el = c }}
        className="video"
        style={this.style}
        dangerouslySetInnerHTML={{ __html: this.props.data.html }}
      />
    )
  }
}

export default Video
