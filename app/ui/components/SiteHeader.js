/* @flow weak */
import React from 'react'
import { browserHistory } from 'react-router'
import { default as cx } from 'classnames'
import MenuNav from 'menu/components/MenuNav'
import MenuTitle from 'menu/components/MenuTitle'
import { isMobile } from 'core/dom'

type Props = {
  isMobile: boolean,
  isOpen: boolean,
  onHeaderClick: Function,
  onNavClick: Function,
  sectionNav?: React$Element<*>,
  sectionTitle?: React$Element<*>,
}

const SiteHeader = (props: Props) => (
  <header id="main-header" className={cx({ isOpen: props.isOpen })}>
    <h1 className="main-title">
      <a onClick={props.onHeaderClick}>
        <span className="site-title">Sandeep Mukherjee</span>
        <span className="section-title">{props.sectionTitle
            ? props.sectionTitle
            : <MenuTitle />
        }</span>
      </a>
    </h1>

    <div className="header-navs">
      <div className="nav-container" onClick={props.onNavClick}>
        <MenuNav />
        {props.sectionNav}
      </div>
    </div>
  </header>
)

type ContainerProps = {
  sectionNav?: React$Element<*>,
}

export class SiteHeaderContainer extends React.Component {
  props: ContainerProps

  state = {
    isMobile: false,
    isOpen: false,
  }

  el = null

  componentWillMount() {
    this.checkMobile()
  }

  componentDidMount() {
    this.checkMobile()
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  checkMobile() {
    if (isMobile()) {
      if (!this.state.isMobile) {
        this.setState({ isMobile: true })
      }
    }
    else if (this.state.isMobile) {
      this.setState({ isMobile: false })
    }
  }

  openMenu() {
    this.setState({ isOpen: true })
  }

  closeMenu() {
    const navs = this.el.querySelector('.header-navs')
    navs.scrollTop = 0
    this.setState({ isOpen: false })
  }

  toggleMenu() {
    if (this.state.isOpen) {
      this.closeMenu()
    }
    else {
      this.openMenu()
    }
  }

  handleResize = () => {
    this.checkMobile()
  }

  handleHeaderClick = () => {
    if (this.state.isMobile) {
      this.toggleMenu()
    }
    else {
      browserHistory.push('/')
    }
  }

  handleNavClick = e => {
    if (e.target.nodeName === 'A') {
      this.closeMenu()
    }
  }

  render() {
    return (
      <span ref={el => { this.el = el }}>
        <SiteHeader
          {...this.props}
          isOpen={this.state.isOpen}
          isMobile={this.state.isMobile}
          onHeaderClick={this.handleHeaderClick}
          onNavClick={this.handleNavClick}
        />
      </span>
    )
  }
}

export default SiteHeaderContainer
