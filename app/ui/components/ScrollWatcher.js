/* @flow weak */
/* eslint react/no-find-dom-node: 0 */
import React from 'react'
import scrollMonitor from 'scrollmonitor'

type Props = {
  children?: React$Element<*>,
  onEnter: Function,
  onExit: Function,
}

export class ScrollWatcher extends React.Component {
  props: Props

  static defaultProps = {
    onEnter: () => {},
    onExit: () => {},
  }

  watcher = null
  el = null

  componentDidMount() {
    this.watcher = scrollMonitor.create(this.el)
    this.watcher.fullyEnterViewport(this.enterViewport)
    this.watcher.exitViewport(this.exitViewport)
  }

  componentWillUnmount() {
    this.watcher.destroy()
  }

  enterViewport = () => {
    this.props.onEnter()
  }

  exitViewport = () => {
    this.props.onExit()
  }

  render() {
    return React.Children.map(this.props.children, child => (
      React.cloneElement(child, { ref: el => { this.el = el } })
    ))[0]
  }

}

export default ScrollWatcher
