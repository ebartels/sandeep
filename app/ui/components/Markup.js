/* @flow weak */
/* eslint react/no-danger: 0 */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { browserHistory } from 'react-router'

type Props = {
  children?: string,
  removeEmptyTags?: boolean
}


export class Markup extends React.Component {
  props: Props
  el: HTMLElement

  static defaultProps = {
    children: '',
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  componentDidMount() {
    this.removeEmptyNodes()
    this.attachLinkListeners()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.children !== this.props.children) {
      this.removeEmptyNodes()
      this.attachLinkListeners()
    }
  }

  removeEmptyNodes() {
    if (!this.props.removeEmptyTags) {
      return
    }

    const nodes = this.el.querySelectorAll('p, div')
    Array.from(nodes).forEach(node => {
      if (node.textContent.trim() === '' && node.childNodes.length <= 1) {
        node.parentNode.removeChild(node)
      }
    })
  }

  attachLinkListeners() {
    const anchors = this.el.querySelectorAll('a')
    Array.from(anchors)
      .filter(a => (
        a.pathname && a.host === location.host
      ))
      .forEach(a => {
        a.removeEventListener('click', this.handleAnchorClick)
        a.addEventListener('click', this.handleAnchorClick)
      })
  }

  handleAnchorClick = e => {
    e.preventDefault()
    const anchor = e.currentTarget
    browserHistory.push(anchor.pathname)
  }

  get html() {
    return this.props.children
  }

  render() {
    return (
      <span
        ref={el => { this.el = el }}
        dangerouslySetInnerHTML={{ __html: this.html }}
      />
    )
  }
}

export default Markup
