/* eslint jsx-a11y/img-has-alt: 0 */
import React from 'react'
import throttle from 'lodash.throttle'

const BLANK_SRC = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='

type Props = {
  src: 'string'
}

export class MasonryImage extends React.Component {
  props: Props

  state = {
    isLoaded: false,
  }

  componentDidMount() {
    this.setImgHeight()
    this.checkVisible()
    setTimeout(this.checkVisible)

    window.addEventListener('resize', this.handleResize)
    window.addEventListener('scroll', this.handleScroll)
  }

  componentDidUpdate() {
    this.setImgHeight()
    this.checkVisible()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleResize = () => {
    this.setImgHeight()
    setTimeout(this.checkVisible, 100)
  }

  handleScroll = throttle(() => {
    this.checkVisible()
  }, 300, { leading: true, trailing: true })

  setImgHeight = () => {
    const width = parseInt(this.img.getAttribute('width'), 10)
    const height = parseInt(this.img.getAttribute('height'), 10)
    const ratio = height / width

    const imgHeight = this.img.width * ratio
    this.img.style.height = `${imgHeight}px`
  }

  checkVisible = () => {
    if (!this.state.isLoaded && this.img) {
      const bbox = this.img.getBoundingClientRect()
      const offset = 2
      const topThresh = window.innerHeight * offset
      const bottomThresh = window.innerHeight * -1
      if (bbox.top < topThresh && bbox.bottom > bottomThresh) {
        this.setState({ isLoaded: true })
      }
    }
  }

  get imgSrc() {
    if (this.state.isLoaded) {
      return this.props.src
    }
    return BLANK_SRC
  }

  render() {
    const props = { ...this.props }
    props.src = this.imgSrc
    return (
      <img {...props} ref={img => { this.img = img }} />
    )
  }
}

export default MasonryImage
