/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import Icon from 'ui/components/Icon'

let nModals = 0

type Props = {
  children?: React$Element<*>,
  onClose?: () => void,
}

export const Modal = (props: Props) => (
  <div className="modal">
    <button className="modal-close" onClick={props.onClose}><Icon name="close" /></button>
    <div className="modal-content">
      {props.children}
    </div>
  </div>
)

Modal.defaultProps = {
  onClose: () => {},
}


type ContainerProps = {
  returnTo?: string,
  routeHistory?: Object[],
}

class ModalContainer extends React.Component {
  props: ContainerProps

  static defaultProps = {
    returnTo: '/',
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onKeyDown)
    document.body.classList.add('modal-open')
    nModals += 1
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyDown)
    nModals -= 1
    if (nModals <= 0) {
      document.body.classList.remove('modal-open')
    }
  }

  onKeyDown = e => {
    if (e.which === 27) {  // escape
      this.close()
    }
  }

  close = () => {
    const { routeHistory, returnTo } = this.props

    // Call history back to the rotue that originally opened the Modal component
    if (routeHistory) {
      const n = routeHistory.findIndex(route => (
        route.pathname === returnTo
      ))

      n > 0
        ? browserHistory.go(n * -1)
        : browserHistory.push(returnTo)
    }
    else {
      browserHistory.push(returnTo)
    }
  }

  render() {
    return <Modal {...this.props} onClose={this.close} />
  }
}

const mapStateToProps = (state, props) => ({
  routeHistory: state.routeHistory,
})

export default connect(mapStateToProps, null, null, {
  withRef: true,
})(ModalContainer)
