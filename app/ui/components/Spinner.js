/* @flow */
import React from 'react'

type Props = {
  delay?: number,
}

type State = {
  show: boolean,
}

export class Spinner extends React.Component {
  props: Props
  timeout: ?number

  static defaultProps = {
    delay: 350,
  }

  state: State = {
    show: false,
  }

  componentDidMount() {
    this.show()
  }

  componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout)
    }
  }

  show = () => {
    this.timeout = setTimeout(() => {
      this.timeout = null
      this.setState({
        show: true,
      })
    }, this.props.delay || 0)
  }

  render() {
    if (!this.state.show) {
      return null
    }
    return (
      <div className="loading-spinner"><div className="spinner" /></div>
    )
  }
}

export default Spinner
