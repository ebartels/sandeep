/* @flow */
import React from 'react'
import { Link } from 'react-router'

export const NotFound = () => (
  <div id="not-found-view" className="error-view">
    <h1 className="large">404 Not Found</h1>

    <p>
      Sorry, we couldn&apos;t find the page you landed on.<br />
      <Link to="/">Go to the Home Page</Link>
    </p>
  </div>
)

export default NotFound
