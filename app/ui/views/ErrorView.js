/* @flow */
import React from 'react'
import { Link } from 'react-router'
import NotFound from 'ui/views/NotFound'

type Props = {
  status: number,
}

export const ErrorView = (props: Props) => (
  <div id="error-view" className="error-view">
    <h1 className="large">{props.status || null} Error</h1>

    <p>
      Sorry, something went wrong.<br />
      <Link to="/">Go to the Home Page</Link>
    </p>
  </div>
)

export const ErrorViewContainer = (props: Props) => (
  props.status === 404
    ? <NotFound />
    : <ErrorView status={props.status} />
)

ErrorView.defaultProps = {
  status: 500,
}

export default ErrorViewContainer
