/* @flow */
import React from 'react'
import DocumentTitle from 'ui/components/DocumentTitle'
import SiteHeader from 'ui/components/SiteHeader'

type Props = {
  content?: React$Element<*>,
  sectionNav?: React$Element<*>,
  sectionTitle?: React$Element<*>,
}

const MainLayout = ({ content, sectionNav, sectionTitle }: Props) => (
  <DocumentTitle>
    <div id="main-layout">
      <SiteHeader sectionNav={sectionNav} sectionTitle={sectionTitle} />

      <div className="main-view">
        {content}
      </div>
    </div>
  </DocumentTitle>
)

export default MainLayout
