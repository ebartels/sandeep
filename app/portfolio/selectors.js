/* @flow weak */
import { createSelector } from 'reselect'

/**
 * Returns the state atom for portfolio module
 */
export const getState = state => state.portfolio


/**
 * Return state atom for all Categories
 */
export const getCategoryState = state => getState(state).categories


/**
 * Returns the category slug from state
 * This will be in props or params
 */
export const getCategorySlug = (state, props) => (
  props.category_slug || (props.params && props.params.category_slug)
)

/**
 * Returns category data (including fetch state)
 */
export const getCategoryData = createSelector(
  [getCategoryState, getCategorySlug],
  (state, slug) => state[slug]
)


/**
 * Returns category based on given slug
 */
export const getCategory = createSelector(
  [getCategoryData, getCategorySlug],
  (data, slug) => (
    (data && !data.isFetching) ? data : null
  )
)

/**
 * Returns the active collection slug for the active category
 */
export const getActiveCollectionSlug = createSelector(
  [getState, getCategorySlug],
  (state, categorySlug) => (
    state.activeCollection[categorySlug]
  )
)
