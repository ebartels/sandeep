/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { selectors } from 'portfolio'

type Props = {
  activeCollectionSlug: string,
  category: Object,
}

export const CategoryTitle = ({ category, activeCollectionSlug }: Props) => {
  if (!category || category.error) {
    return null
  }
  if (category.collections.length < 2) {
    return null
  }

  return <span>{category.collections.find(c => c.slug === activeCollectionSlug).title}</span>
}

const mapStateToProps = (state, props) => ({
  category: selectors.getCategory(state, props),
  activeCollectionSlug: selectors.getActiveCollectionSlug(state, props),
})

export default connect(mapStateToProps)(CategoryTitle)
