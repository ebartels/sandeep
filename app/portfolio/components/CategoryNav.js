/* @flow weak */
import React from 'react'
import { scrollToElement } from 'core/dom'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import Link from 'react-router/lib/Link'
import IndexLink from 'react-router/lib/IndexLink'
import { setActiveCollectionSlug } from 'portfolio/actions'
import { selectors } from 'portfolio'

type Props = {
  category: Object,
  activeCollectionSlug: string,
  onLinkClick: () => {}
}

export const CategoryNav = ({ category, ...props }: Props) => (
  <nav className="nav-menu sub-nav collection-sub-nav">

    <div className="horizontal-links">
      <Link to={`/portfolio/${category.slug}/detail`} activeClassName="active">1x1</Link>
      <span className="sep">|</span>
      <IndexLink to={`/portfolio/${category.slug}/`} activeClassName="active">Grid</IndexLink>
    </div>

    <ul>
      {category.collections.map((c, i) => (
        <li key={i}>
          <a
            onClick={() => { props.onLinkClick(c.slug) }}
            className={c.slug === props.activeCollectionSlug ? 'active' : ''}>
            {c.title}
          </a>
        </li>
      ))}
    </ul>
  </nav>
)


type ContainerProps = {
  category: Object,
  activeCollectionSlug: string,
  dispatch: Function,
}

export class CategoryNavContainer extends React.Component {
  props: ContainerProps

  handleLinkClick = (slug) => {
    // Set url location
    const url = `/portfolio/${this.props.category.slug}/`
    if (window.location.pathname !== url) {
      browserHistory.push(url)
    }

    // Update to active collection
    this.setActiveCollectionSlug(slug)

    // Handle animated scroll
    scrollToElement(`#collection-${slug}`, {
      duration: 800,
      callback: () => {
        setTimeout(() => { this.setActiveCollectionSlug(slug) }, 50)
      },
    })
  }

  setActiveCollectionSlug = slug => {
    this.props.dispatch(setActiveCollectionSlug(this.props.category.slug, slug))
  }

  render() {
    const { category } = this.props

    if (!category || category.error) {
      return null
    }

    if (category.collections.length < 2) {
      return null
    }

    return (
      <CategoryNav
        category={category}
        activeCollectionSlug={this.props.activeCollectionSlug}
        onLinkClick={this.handleLinkClick}
      />
    )
  }
}

const mapStateToProps = (state, props) => ({
  category: selectors.getCategory(state, props),
  activeCollectionSlug: selectors.getActiveCollectionSlug(state, props),
})

export default connect(mapStateToProps)(CategoryNavContainer)
