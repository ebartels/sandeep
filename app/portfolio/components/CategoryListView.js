/* @flow weak */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { connect } from 'react-redux'
import { scrollToElement } from 'core/dom'
import CollectionList from 'portfolio/components/CollectionList'
import { setActiveCollectionSlug } from 'portfolio/actions'
import { selectors } from 'portfolio'

type Props = {
  category: Object,
  onLayoutComplete: () => {},
  onCollectionEnter: () => {},
}

export const CategoryListView = (props: Props) => (
  <CollectionList
    category={props.category}
    collections={props.category.collections}
    onLayoutComplete={props.onLayoutComplete}
    onCollectionEnter={props.onCollectionEnter}
  />
)

type ContainerProps = {
  category: Object,
  activeCollectionSlug: string,
  dispatch: Function,
}

export class CategoryListViewContainer extends React.Component {
  props: ContainerProps

  state = {
    watchCollectionScroll: false,
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  scrollToCollection = () => {
    setTimeout(() => {
      scrollToElement(`#collection-${this.props.activeCollectionSlug}`)
    })
  }

  onCollectionEnter = (slug) => {
    if (this.state.watchCollectionScroll) {
      this.props.dispatch(setActiveCollectionSlug(this.props.category.slug, slug))
    }
  }

  handleLayoutComplete = () => {
    this.scrollToCollection()
    setTimeout(() => {
      this.setState({ watchCollectionScroll: true })
    }, 500)
  }

  render() {
    return (
      <CategoryListView
        category={this.props.category}
        onCollectionEnter={this.onCollectionEnter}
        onLayoutComplete={this.handleLayoutComplete}
      />
    )
  }
}

const mapStateToProps = (state, props) => ({
  activeCollectionSlug: selectors.getActiveCollectionSlug(state, {
    category_slug: props.category.slug,
  }),
})

export default connect(mapStateToProps)(CategoryListViewContainer)
