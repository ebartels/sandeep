/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import DocumentTitle from 'ui/components/DocumentTitle'
import Modal from 'ui/components/Modal'
import MediaView from 'ui/components/MediaView'
import { setActiveCollectionSlug } from 'portfolio/actions'
import { selectors } from 'portfolio'

type Props = {
  category: Object,
  items: Object[],
  media_id?: string,
  onShowItem?: (i: number) => void,
}

export const CategoryDetailView = (props: Props) => (
  <div className="detail-view category-detail-view">
    <Modal returnTo={`/portfolio/${props.category.slug}/`}>
      <MediaView
        items={props.items}
        activeId={props.media_id}
        onShowItem={props.onShowItem}
      />
    </Modal>
  </div>
)

type ContainerProps = {
  category: Object,
  media_id?: string,
  activeCollectionSlug?: string,
  dispatch: Function,
}

class CategoryDetailViewContainer extends React.Component {
  props: ContainerProps

  get mediaItems() {
    const items = []
    this.props.category.collections.forEach(collection => {
      collection.images.forEach(image => {
        items.push({
          ...image,
          collection: collection.slug,
        })
      })
    })
    return items
  }

  get mediaId() {
    if (this.props.media_id) {
      return parseInt(this.props.media_id, 10)
    }

    const activeCollection = this.props.category.collections.find(c => (
      c.slug === this.props.activeCollectionSlug
    ))
    if (activeCollection && activeCollection.images.length) {
      return activeCollection.images[0].id
    }

    return null
  }

  checkActiveCollection = () => {
    const currentItem = this.mediaItems.find(item => item.id === this.mediaId)
    if (currentItem && currentItem.collection !== this.props.activeCollectionSlug) {
      this.props.dispatch(setActiveCollectionSlug(this.props.category.slug, currentItem.collection))
    }
  }

  handleShowItem = (itemId) => {
    // Update url for item id
    const url = `/portfolio/${this.props.category.slug}/detail/${itemId}`
    browserHistory.replace(url)

    // Update active collection
    this.checkActiveCollection()
  }

  render() {
    const { category } = this.props

    if (!category || category.error) {
      return null
    }

    return (
      <DocumentTitle title={category.name}>
        <CategoryDetailView
          category={category}
          items={this.mediaItems}
          media_id={this.mediaId}
          onShowItem={this.handleShowItem}
        />
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state, props) => ({
  category: selectors.getCategory(state, props),
  media_id: props.params.media_id,
  activeCollectionSlug: selectors.getActiveCollectionSlug(state, props),
})

export default connect(mapStateToProps)(CategoryDetailViewContainer)
