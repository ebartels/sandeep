/* @flow weak */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { default as cx } from 'classnames'
import debounce from 'lodash.debounce'
import ScrollWatcher from 'ui/components/ScrollWatcher'
import ImageList from 'ui/components/ImageList'

type Props = {
  collections: Array<Object>,
  onCollectionEnter: () => {},
  onLayoutComplete?: () => {},
}

export const CollectionList = ({ collections, ...props }: Props) => (
  <div className="collection-list">
    {collections.map((collection, i) => (
      <ScrollWatcher
        key={collection.slug}
        onEnter={() => { props.onCollectionEnter(collection.slug) }}>
        <section
          className={cx('collection-item', {
            'last-item': (i === collections.length - 1),
          })}
          id={`collection-${collection.slug}`}>

          <h2>{collection.title}</h2>

          <ImageList
            images={collection.images}
            onLayoutComplete={props.onLayoutComplete}
          />
        </section>
      </ScrollWatcher>
    ))}
  </div>
)

type ContainerProps = {
  collections: Array<Object>,
  onCollectionEnter: () => {},
  onLayoutComplete?: () => {},
}

export class CollectionListContainer extends React.Component {
  props: ContainerProps

  nComplete = 0

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  handleLayoutComplete = () => {
    if (this.props.onLayoutComplete) {
      this.nComplete += 1
      if (this.nComplete === this.props.collections.length) {
        this.props.onLayoutComplete()
      }
    }
  }

  onCollectionEnter = debounce(this.props.onCollectionEnter, 250, {
    leading: false,
    trailing: true,
  })

  render() {
    return (
      <CollectionList
        collections={this.props.collections}
        onLayoutComplete={this.handleLayoutComplete}
        onCollectionEnter={this.onCollectionEnter}
      />
    )
  }
}

export default CollectionListContainer
