/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import ErrorView from 'ui/views/ErrorView'
import Spinner from 'ui/components/Spinner'
import DocumentTitle from 'ui/components/DocumentTitle'
import BodyClass from 'ui/components/BodyClass'
import CategoryListView from 'portfolio/components/CategoryListView'
import { fetchCategoryIfNeeded } from 'portfolio/actions'
import { selectors } from 'portfolio'

type Props = {
  category: Object,
}

export const CategoryView = (props: Props) => (
  <div id="category-index-view">
    <CategoryListView category={props.category} />
  </div>
)

type ContainerProps = {
  category: Object,
  category_slug: string,
  dispatch: Function,
  children?: React$Element<*>,
}

export class CategoryViewContainer extends React.Component {
  props: ContainerProps

  fetchCategory(slug) {
    this.props.dispatch(fetchCategoryIfNeeded(slug))
  }

  componentDidMount() {
    this.fetchCategory(this.props.category_slug)
  }

  componentWillReceiveProps(newProps) {
    if (newProps.category_slug !== this.props.category_slug) {
      this.fetchCategory(newProps.category_slug)
    }
  }

  render() {
    const { category, children } = this.props
    if (!category) {
      return <Spinner />
    }
    if (category.error) {
      return <ErrorView status={category.error.status} />
    }

    return (
      <DocumentTitle title={category.name}>
        <BodyClass className={`category-${category.slug}`}>
          <span>
            <CategoryView category={category} />
            {children}
          </span>
        </BodyClass>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state, props) => ({
  category: selectors.getCategory(state, props),
  category_slug: selectors.getCategorySlug(state, props),
})

export default connect(mapStateToProps)(CategoryViewContainer)
