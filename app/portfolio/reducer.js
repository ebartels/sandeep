/* @flow weak */
import { combineReducers } from 'redux'
import * as t from './actionTypes'

export function categories(state = {}, action) {
  switch (action.type) {
    case t.FETCH_CATEGORY_REQUEST: {
      return {
        ...state,
        [action.payload]: {
          isFetching: true,
        },
      }
    }
    case t.FETCH_CATEGORY_SUCCESS: {
      return {
        ...state,
        [action.payload.slug]: action.payload,
      }
    }
    case t.FETCH_CATEGORY_FAILURE: {
      return {
        ...state,
        [action.payload.slug]: { error: action.payload.error },
      }
    }
    default:
      return state
  }
}

export function activeCollection(state = {}, action) {
  switch (action.type) {
    case t.SET_ACTIVE_COLLECTION_SLUG: {
      return {
        ...state,
        [action.payload.categorySlug]: action.payload.slug,
      }
    }
    default:
      return state
  }
}

export default combineReducers({
  categories,
  activeCollection,
})
