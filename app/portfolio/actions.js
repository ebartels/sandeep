/* @flow weak */
import fetchUrl from 'core/fetch'
import * as t from './actionTypes'
import * as selectors from './selectors'

/**
 * Sets the active collection slug
 */
export const setActiveCollectionSlug = (categorySlug, slug) => ({
  type: t.SET_ACTIVE_COLLECTION_SLUG,
  payload: { categorySlug, slug },
})

/**
 * Fetch a category by slug from the api
 */
export const fetchCategory = (slug) => async (dispatch) => {
  try {
    dispatch({ type: t.FETCH_CATEGORY_REQUEST, payload: slug })
    const data = await fetchUrl(`portfolio/category/${slug}`)

    // Set the active collection slug
    if (data.collections && data.collections.length) {
      dispatch(setActiveCollectionSlug(data.slug, data.collections[0].slug))
    }

    dispatch({
      type: t.FETCH_CATEGORY_SUCCESS,
      payload: data,
    })
  }
  catch (error) {
    dispatch({
      type: t.FETCH_CATEGORY_FAILURE,
      payload: { slug, error: { status: error.status } },
    })
  }
}

/**
 * Fetch a category by slug, but only if it is not available in state
 */
export const fetchCategoryIfNeeded = (slug) => async (dispatch, getState) => {
  const existing = selectors.getCategoryData(getState(), { category_slug: slug })
  if (!existing && !(existing || {}).isFetching) {
    dispatch(fetchCategory(slug))
  }
}
