/* @flow */
import 'styles'

import React from 'react'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import configureStore from './store/configureStore'
import routes, { routerMiddleware } from './routes'

const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)


export const App = () => (
  <Provider store={store}>
    <Router
      history={history}
      render={routerMiddleware}>
      {routes}
    </Router>
  </Provider>
)

export default App
