/* @flow weak */
import fetchUrl from 'core/fetch'
import * as t from './actionTypes'
import * as selectors from './selectors'


/**
 * Fetch homepage images from api
 */
export const fetchHomepageImages = () => async (dispatch) => {
  try {
    dispatch({ type: t.FETCH_HOMEPAGE_IMAGES_REQUEST })
    const data = await fetchUrl('local/homepageimage')
    dispatch({ type: t.FETCH_HOMEPAGE_IMAGES_SUCCESS, payload: data })
  }
  catch (error) {
    dispatch({
      type: t.FETCH_HOMEPAGE_IMAGES_FAILURE,
      payload: { error: { status: error.status } },
    })
  }
}

/**
 * Fetch homepage images but only if not in state
 */
export const fetchHomepageImagesIfNeeded = () => async (dispatch, getState) => {
  const existing = selectors.getHomepageImages(getState())
  if (!existing || !existing.length) {
    dispatch(fetchHomepageImages())
  }
}
