/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import ErrorView from 'ui/views/ErrorView'
import Spinner from 'ui/components/Spinner'
import DocumentTitle from 'ui/components/DocumentTitle'
import BodyClass from 'ui/components/BodyClass'
import { fetchHomepageImagesIfNeeded } from 'homepage/actions'
import { selectors } from 'homepage'

type Props = {
  image: Object,
  onPageClick: (Event) => {},
}

export const HomeView = ({ image, ...props }: Props) => (
  <div id="home-view" onClick={props.onPageClick} onTouchMove={props.onPageClick}>
    <div
      className="image-bg"
      style={{
        backgroundImage: `url(${image.resized.large.url})`,
      }}
    />
  </div>
)

HomeView.defaultProps = {
}

type ContainerProps = {
  images: any,
  dispatch: Function,
}

export class HomeViewContainer extends React.Component {
  props: ContainerProps

  fetchHomepageImages() {
    this.props.dispatch(fetchHomepageImagesIfNeeded())
  }

  componentDidMount() {
    this.fetchHomepageImages()
  }

  onPageClick = (e) => {
    browserHistory.push('/portfolio/works/')
  }

  get randomImage() {
    const { images } = this.props
    return images[Math.floor(Math.random() * images.length)].image
  }

  render() {
    const { images } = this.props

    if (!images.length) {
      return <Spinner />
    }
    if (images.error) {
      return <ErrorView status={images.error.status} />
    }

    return (
      <DocumentTitle title="Exhibitions">
        <BodyClass className="homepage-body">
          <HomeView image={this.randomImage} onPageClick={this.onPageClick} />
        </BodyClass>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state, props) => ({
  images: selectors.getHomepageImages(state),
})

export default connect(mapStateToProps)(HomeViewContainer)
