/* @flow weak */

/**
 * Returns the state atom for homepage module
 */
export const getState = state => state.homepage


/**
 * Returns images for homepage
 */
export const getHomepageImages = state => getState(state).images
