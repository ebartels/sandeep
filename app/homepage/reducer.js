import { combineReducers } from 'redux'
import * as t from './actionTypes'

export function images(state = [], action) {
  switch (action.type) {
    case t.FETCH_HOMEPAGE_IMAGES_REQUEST: {
      return {
        isFetching: true,
      }
    }
    case t.FETCH_HOMEPAGE_IMAGES_SUCCESS: {
      return action.payload
    }
    case t.FETCH_HOMEPAGE_IMAGES_FAILURE: {
      return {
        error: action.payload.error,
      }
    }
    default:
      return state
  }
}

export default combineReducers({
  images,
})
