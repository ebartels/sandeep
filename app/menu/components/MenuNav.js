/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { default as Link } from 'ui/components/NavLink'
import { fetchMenuIfNeeded } from 'menu/actions'
import { selectors } from 'menu'

type Props = {
  menu: Object[],
  subMenu?: Object[],
  currentRootMenuItem?: Object,
}

export const MenuNavView = ({ menu, subMenu, currentRootMenuItem }: Props) => (
  <div id="menu-index-view">
    <nav className="nav-menu main-menu-nav">
      <ul>
        {menu.map((item, i) => (
          <li key={i}>
            <Link
              to={item.url}
              className={currentRootMenuItem && item.url === currentRootMenuItem.url ? 'active' : ''}>{item.title}</Link>
          </li>
        ))}
      </ul>
    </nav>

    {subMenu && subMenu.length ?
      <nav className="nav-menu main-menu-subnav">
        <ul>
          {subMenu.map((item, i) => (
            <li key={i}><Link to={item.url}>{item.title}</Link></li>
          ))}
        </ul>
      </nav>
    : null }
  </div>
)

type ContainerProps = {
  menu: Object[],
  currentRootMenuItem: Object,
  dispatch: Function,
}

export class MenuNavViewContainer extends React.Component {
  props: ContainerProps

  fetchMenu() {
    this.props.dispatch(fetchMenuIfNeeded())
  }

  componentDidMount() {
    this.fetchMenu()
  }

  render() {
    const { menu, currentRootMenuItem } = this.props
    if (!menu.length || menu.error) {
      return null
    }

    const subMenu = currentRootMenuItem ? currentRootMenuItem.children : []

    return (
      <MenuNavView
        menu={menu}
        subMenu={subMenu}
        currentRootMenuItem={currentRootMenuItem}
      />
    )
  }
}

const mapStateToProps = (state, props) => ({
  menu: selectors.getMenu(state),
  currentRootMenuItem: selectors.getCurrentRootMenuItem(state),
})

export default connect(mapStateToProps)(MenuNavViewContainer)
