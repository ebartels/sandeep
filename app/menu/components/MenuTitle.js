/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { selectors } from 'menu'

type Props = {
  currentMenuItem: Object,
}

export const MenuTitle = ({ currentMenuItem }: Props) => (
  currentMenuItem
    ? <span>{currentMenuItem.title}</span>
    : null
)

const mapStateToProps = (state, props) => ({
  currentMenuItem: selectors.getCurrentMenuItem(state),
})

export default connect(mapStateToProps)(MenuTitle)
