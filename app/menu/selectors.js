/* @flow weak */
import { createSelector } from 'reselect'

/**
 * Returns the state atom for menu module
 */
export const getState = state => state.menu


/**
 * Return state atom for menu
 */
export const getMenuState = state => getState(state).menu


/**
 * Returns menu data (including fetch state)
 */
export const getMenu = createSelector(
  [getMenuState],
  (state) => state
)

/**
 * Return current route History
 */
export const getRouteHistory = state => state.routeHistory

/**
 * Returns the current menu item based on the route
 */

export const getCurrentMenuItem = createSelector(
  [getMenu, getRouteHistory],
  (menu, routeHistory) => {
    if (!menu || !menu.length) {
      return null
    }

    const currentRoute = routeHistory[0]
    return menu
      .map(item => item.children)
      .filter(list => list.length > 1)
      .reduce((list, cur) => list.concat(cur), [])
      .find(i => (i.url === currentRoute.pathname))
  }
)

/**
 * Returns the root menu item for the current item
 */
export const getCurrentRootMenuItem = createSelector(
  [getCurrentMenuItem, getMenu],
  (currentMenuItem, menu) => {
    if (!currentMenuItem) {
      return null
    }

    return menu.find(item => item.slug === currentMenuItem.parent)
  }
)
