/* @flow weak */
import { combineReducers } from 'redux'
import * as t from './actionTypes'

export function menu(state = [], action) {
  switch (action.type) {
    case t.FETCH_MENU_REQUEST: {
      return {
        isFetching: true,
      }
    }
    case t.FETCH_MENU_SUCCESS: {
      return action.payload
    }
    case t.FETCH_MENU_FAILURE: {
      return {
        error: action.payload.error,
      }
    }
    default:
      return state
  }
}

export default combineReducers({
  menu,
})
