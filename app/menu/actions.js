/* @flow weak */
import fetchUrl from 'core/fetch'
import * as t from './actionTypes'
import * as selectors from './selectors'

/**
 * Fetch menu from the api
 */
export const fetchMenu = () => async (dispatch) => {
  try {
    dispatch({ type: t.FETCH_MENU_REQUEST })
    const data = await fetchUrl('menu/')
    dispatch({
      type: t.FETCH_MENU_SUCCESS,
      payload: data,
    })
  }
  catch (error) {
    dispatch({
      type: t.FETCH_MENU_FAILURE,
      payload: { error: { status: error.status } },
    })
  }
}

/**
 * Fetch menu, but only if it is not available in state
 */
export const fetchMenuIfNeeded = () => async (dispatch, getState) => {
  const existing = selectors.getMenu(getState())
  if (!existing || !existing.length) {
    dispatch(fetchMenu())
  }
}
