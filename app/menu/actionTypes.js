/* @flow */
export const FETCH_MENU_REQUEST = 'menu/FETCH_MENU_REQUEST'
export const FETCH_MENU_SUCCESS = 'menu/FETCH_MENU_SUCCESS'
export const FETCH_MENU_FAILURE = 'menu/FETCH_MENU_FAILURE'
