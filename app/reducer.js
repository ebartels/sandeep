/* @flow */
import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'
import { routeHistory } from 'routes/reducer'
import { reducer as menu } from 'menu'
import { reducer as homepage } from 'homepage'
import { reducer as pages } from 'pages'
import { reducer as portfolio } from 'portfolio'
import { reducer as exhibitions } from 'exhibitions'

const rootReducer = combineReducers({
  routing,
  routeHistory,
  menu,
  homepage,
  pages,
  portfolio,
  exhibitions,
})

export default rootReducer
