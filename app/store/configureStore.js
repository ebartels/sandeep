import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'

import rootReducer from 'reducer'

function configureStore(initialState) {
  const logger = createLogger({
    collapsed: true,
    predicate: () =>
      process.env.NODE_ENV === 'development',
  })

  const middleware = applyMiddleware(thunkMiddleware, logger)

  const store = middleware(createStore)(rootReducer, initialState,
    window.devToolsExtension ? window.devToolsExtension() : undefined
  )
  return store
}

export default configureStore
