import { applyRouterMiddleware } from 'react-router'
import { useScroll } from 'react-router-scroll'

const routerMiddleware = applyRouterMiddleware(useScroll((prevRouterProps, { routes }) => (
  !routes.some(route => route.ignoreScrollBehavior)
)))

export default routerMiddleware
