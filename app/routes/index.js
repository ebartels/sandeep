/* @flow */
import React from 'react'
import { Route, IndexRoute } from 'react-router'
import MainLayout from 'ui/views/MainLayout'
import NotFound from 'ui/views/NotFound'
import HomeView from 'homepage/views/HomeView'
import CategoryView from 'portfolio/views/CategoryView'
import CategoryNav from 'portfolio/components/CategoryNav'
import CategoryTitle from 'portfolio/components/CategoryTitle'
import CategoryDetailView from 'portfolio/components/CategoryDetailView'
import ExhibitionIndexView from 'exhibitions/views/ExhibitionIndexView'
import ExhibitionsNav from 'exhibitions/components/ExhibitionsNav'
import ExhibitionsTitle from 'exhibitions/components/ExhibitionsTitle'
import ExhibitionsDetailView from 'exhibitions/components/ExhibitionsDetailView'
import PageView from 'pages/views/PageView'

export { default as routerMiddleware } from './routerMiddleware'

export default (
  <Route path="/" component={MainLayout}>
    {/* Home page */}
    <IndexRoute components={{ content: HomeView }} />

    {/* Portfolio */}
    <Route
      path="portfolio/:category_slug"
      components={{
        content: CategoryView,
        sectionNav: CategoryNav,
        sectionTitle: CategoryTitle,
      }}
      ignoreScrollBehavior>
      <Route path="detail" component={CategoryDetailView}>
        <Route path=":media_id" />
      </Route>
    </Route>

    {/* Exhibitions */}
    <Route
      path="exhibitions"
      components={{
        content: ExhibitionIndexView,
        sectionNav: ExhibitionsNav,
        sectionTitle: ExhibitionsTitle,
      }}
      ignoreScrollBehavior>
      <Route path="detail" component={ExhibitionsDetailView}>
        <Route path=":media_id" />
      </Route>
    </Route>

    {/* Pages */}
    <Route path="pages/:page_slug" components={{ content: PageView }} />

    {/* 404 */}
    <Route path="*" components={{ content: NotFound }} />
  </Route>
)
