/* @flow weak */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { connect } from 'react-redux'
import { scrollToElement } from 'core/dom'
import ExhibitionList from 'exhibitions/components/ExhibitionList'
import { setActiveYear } from 'exhibitions/actions'
import { selectors } from 'exhibitions'

type Props = {
  exhibitions: Object[],
  onLayoutComplete: () => {},
  onExhibitionEnter: () => {},
}

export const ExhibitionListView = (props: Props) => (
  <ExhibitionList
    exhibitions={props.exhibitions}
    onLayoutComplete={props.onLayoutComplete}
    onExhibitionEnter={props.onExhibitionEnter}
  />
)

type ContainerProps = {
  exhibitions: Object[],
  activeYear: string,
  dispatch: Function,
}

export class ExhibitionListViewContainer extends React.Component {
  props: ContainerProps

  state = {
    watchExhibitionScroll: false,
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  scrollToYear = () => {
    setTimeout(() => {
      scrollToElement(`#year-${this.props.activeYear}`)
    })
  }

  onExhibitionEnter = (year) => {
    if (this.state.watchExhibitionScroll) {
      this.props.dispatch(setActiveYear(year))
    }
  }

  handleLayoutComplete = () => {
    this.scrollToYear()
    setTimeout(() => {
      this.setState({ watchExhibitionScroll: true })
    }, 500)
  }

  render() {
    return (
      <ExhibitionListView
        exhibitions={this.props.exhibitions}
        onExhibitionEnter={this.onExhibitionEnter}
        onLayoutComplete={this.handleLayoutComplete}
      />
    )
  }
}

const mapStateToProps = (state, props) => ({
  activeYear: selectors.getActiveYear(state),
})

export default connect(mapStateToProps)(ExhibitionListViewContainer)
