
/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import DocumentTitle from 'ui/components/DocumentTitle'
import Modal from 'ui/components/Modal'
import MediaView from 'ui/components/MediaView'
import { setActiveYear } from 'exhibitions/actions'
import { selectors } from 'exhibitions'

type Props = {
  items: Object[],
  media_id?: string,
  onShowItem?: (i: number) => void,
}

export const ExhibitionsDetailView = (props: Props) => (
  <div className="detail-view exhibition-detail-view">
    <Modal returnTo="/exhibitions/">
      <MediaView
        items={props.items}
        activeId={props.media_id}
        onShowItem={props.onShowItem}
      />
    </Modal>
  </div>
)

type ContainerProps = {
  exhibitions: Object[],
  media_id?: string,
  activeYear?: number,
  dispatch: Function,
}

class ExhibitionsDetailViewContainer extends React.Component {
  props: ContainerProps

  componentDidMount() {
    this.checkActiveYear()
  }

  get mediaItems() {
    const items = []
    this.props.exhibitions.forEach(exhibition => {
      exhibition.images.forEach(image => {
        items.push({
          ...image,
          caption: `${image.caption} <p>${exhibition.title}<br>${exhibition.location}</p>`,
          exhibition,
        })
      })
    })
    return items
  }

  get mediaId() {
    if (this.props.media_id) {
      return parseInt(this.props.media_id, 10)
    }

    const activeExhibition = this.props.exhibitions.find(e => (
      e.year === this.props.activeYear
    ))
    if (activeExhibition && activeExhibition.images.length) {
      return activeExhibition.images[0].id
    }

    return null
  }

  checkActiveYear = () => {
    const currentItem = this.mediaItems.find(item => item.id === this.mediaId)
    if (currentItem && currentItem.exhibition.year !== this.props.activeYear) {
      this.props.dispatch(setActiveYear(currentItem.exhibition.year))
    }
  }

  handleShowItem = (itemId) => {
    // Update url for item id
    const url = `/exhibitions/detail/${itemId}`
    browserHistory.replace(url)

    // Update active year
    this.checkActiveYear()
  }

  render() {
    const { exhibitions } = this.props

    if (!exhibitions.length || exhibitions.error) {
      return null
    }

    return (
      <DocumentTitle title="Exhibitions">
        <ExhibitionsDetailView
          exhibitions={exhibitions}
          items={this.mediaItems}
          media_id={this.mediaId}
          onShowItem={this.handleShowItem}
        />
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state, props) => ({
  exhibitions: selectors.getExhibitions(state, props),
  media_id: props.params.media_id,
  activeYear: selectors.getActiveYear(state, props),
})

export default connect(mapStateToProps)(ExhibitionsDetailViewContainer)
