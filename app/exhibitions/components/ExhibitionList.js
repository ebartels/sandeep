/* @flow weak */
import React from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { default as cx } from 'classnames'
import debounce from 'lodash.debounce'
import ScrollWatcher from 'ui/components/ScrollWatcher'
import ImageList from 'ui/components/ImageList'

type Props = {
  exhibitions: Array<Object>,
  exhibitionsByYear: Array<Object>,
  onExhibitionEnter: () => {},
  onLayoutComplete?: () => {},
}

export const ExhibitionList = (props: Props) => (
  <div className="collection-list exhibition-list">
    {props.exhibitionsByYear.map(([year, exhibitions], j) => (
      <ScrollWatcher
        key={j}
        onEnter={() => { props.onExhibitionEnter(year) }}>
        <div id={`year-${year}`} key={j}>
          {exhibitions.map((exhibition, i) => (
            <section
              className={cx('collection-item', 'exhibition-item', {
                'last-item': (exhibition.index === props.exhibitions.length - 1),
              })}
              key={exhibition.slug}
              id={`exhibition-${exhibition.slug}`}>

              <h2>
                <span className="title">{exhibition.title}</span>
                &nbsp;|&nbsp;
                <span className="location">{exhibition.location}</span>
              </h2>
              <ImageList
                images={exhibition.images}
                onLayoutComplete={props.onLayoutComplete}
              />
            </section>
          ))}
        </div>
      </ScrollWatcher>
    ))}
  </div>
)

type ContainerProps = {
  exhibitions: Array<Object>,
  onExhibitionEnter: () => {},
  onLayoutComplete: () => {},
}

export class ExhibitionListContainer extends React.Component {
  props: ContainerProps

  nComplete = 0

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState)
  }

  handleLayoutComplete = () => {
    if (this.props.onLayoutComplete) {
      this.nComplete += 1
      if (this.nComplete === this.props.exhibitions.length) {
        this.props.onLayoutComplete()
      }
    }
  }

  onExhibitionEnter = debounce(this.props.onExhibitionEnter, 250, {
    leading: false,
    trailing: true,
  })

  get exhibitionsByYear() {
    return Object.entries(this.props.exhibitions
      .map((e, i) => ({ ...e, index: i }))
      .reduce((grouped, exhibition) => {
        const key = exhibition.year
        return {
          ...grouped,
          [key]: [...(grouped[key] || []), exhibition],
        }
      }, {}))
      .sort((a, b) => (parseInt(b, 10) - parseInt(a, 10)))
  }

  render() {
    return (
      <ExhibitionList
        exhibitions={this.props.exhibitions}
        exhibitionsByYear={this.exhibitionsByYear}
        onLayoutComplete={this.handleLayoutComplete}
        onExhibitionEnter={this.onExhibitionEnter}
      />
    )
  }
}

export default ExhibitionListContainer
