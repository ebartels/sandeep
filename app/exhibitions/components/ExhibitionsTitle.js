/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import { selectors } from 'exhibitions'

type Props = {
  activeYear: string,
}

export const ExhibitionsTitle = ({ activeYear }: Props) => (
  activeYear ? <span>{activeYear}</span> : null
)

const mapStateToProps = (state, props) => ({
  activeYear: selectors.getActiveYear(state, props),
})

export default connect(mapStateToProps)(ExhibitionsTitle)
