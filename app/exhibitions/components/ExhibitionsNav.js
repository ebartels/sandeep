/* @flow weak */
import React from 'react'
import { scrollToElement } from 'core/dom'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import Link from 'react-router/lib/Link'
import IndexLink from 'react-router/lib/IndexLink'
import { setActiveYear } from 'exhibitions/actions'
import { selectors } from 'exhibitions'

type Props = {
  years: string[],
  activeYear: string,
  onLinkClick: () => {},
}

export const ExhibitionsNav = ({ years, ...props }: Props) => (
  <nav className="nav-menu sub-nav exhibitions-sub-nav">

    <div className="horizontal-links">
      <Link to={'/exhibitions/detail'} activeClassName="active">1x1</Link>
      <span className="sep">|</span>
      <IndexLink to={'/exhibitions/'} activeClassName="active">Grid</IndexLink>
    </div>

    <ul>
      {years.map((year, i) => (
        <li key={i}>
          <a
            onClick={() => { props.onLinkClick(year) }}
            className={year === props.activeYear ? 'active' : ''}>
            {year}
          </a>
        </li>
      ))}
    </ul>
  </nav>
)


type ContainerProps = {
  exhibitions: Object,
  activeYear: string,
  dispatch: Function,
}

export class ExhibitionsNavContainer extends React.Component {
  props: ContainerProps

  handleLinkClick = (year) => {
    // Set url location
    const url = '/exhibitions/'
    if (window.location.pathname !== url) {
      browserHistory.push(url)
    }

    // Update to active year
    this.setActiveYear(year)

    // Handle animated scroll
    scrollToElement(`#year-${year}`, {
      duration: 800,
      callback: () => {
        setTimeout(() => { this.setActiveYear(year) }, 50)
      },
    })
  }

  setActiveYear = year => {
    this.props.dispatch(setActiveYear(year))
  }

  render() {
    const { exhibitions } = this.props

    if (!exhibitions.length || exhibitions.error) {
      return null
    }

    const years = exhibitions
      .map(e => e.year)
      .filter((e, i, self) => (self.indexOf(e) === i))

    return (
      <ExhibitionsNav
        years={years}
        activeYear={this.props.activeYear}
        onLinkClick={this.handleLinkClick}
      />
    )
  }
}

const mapStateToProps = (state, props) => ({
  exhibitions: selectors.getExhibitions(state, props),
  activeYear: selectors.getActiveYear(state),
})

export default connect(mapStateToProps)(ExhibitionsNavContainer)
