/* @flow weak */
import { createSelector } from 'reselect'

/**
 * Returns the state atom for exhibitions module
 */
export const getState = state => state.exhibitions


/**
 * Return state atom for all Exhibitions
 */
export const getExhibitionsState = state => getState(state).exhibitions


/**
 * Returns exhibitions data (including fetch state)
 */
export const getExhibitions = createSelector(
  [getExhibitionsState],
  (state) => state
)

/**
 * Returns the active collection slug for the active category
 */
export const getActiveYear = (state) => getState(state).activeYear
