/* @flow weak */
import { combineReducers } from 'redux'
import * as t from './actionTypes'

export function exhibitions(state = [], action) {
  switch (action.type) {
    case t.FETCH_EXHIBITIONS_REQUEST: {
      return {
        isFetching: true,
      }
    }
    case t.FETCH_EXHIBITIONS_SUCCESS: {
      return action.payload
    }
    case t.FETCH_EXHIBITIONS_FAILURE: {
      return {
        error: action.payload.error,
      }
    }
    default:
      return state
  }
}

export function activeYear(state = null, action) {
  switch (action.type) {
    case t.SET_ACTIVE_YEAR: {
      return action.payload
    }
    default:
      return state
  }
}

export default combineReducers({
  exhibitions,
  activeYear,
})
