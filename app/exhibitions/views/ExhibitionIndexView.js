/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import ErrorView from 'ui/views/ErrorView'
import Spinner from 'ui/components/Spinner'
import DocumentTitle from 'ui/components/DocumentTitle'
import ExhibitionListView from 'exhibitions/components/ExhibitionListView'
import { fetchExhibitionsIfNeeded } from 'exhibitions/actions'
import { selectors } from 'exhibitions'

type Props = {
  exhibitions: Object[],
}

export const ExhibitionIndexView = (props: Props) => (
  <div id="exhibition-index-view">
    <ExhibitionListView exhibitions={props.exhibitions} />
  </div>
)

type ContainerProps = {
  exhibitions: any,
  dispatch: Function,
  children?: React$Element<*>,
}

export class ExhibitionIndexViewContainer extends React.Component {
  props: ContainerProps

  fetchExhibitions() {
    this.props.dispatch(fetchExhibitionsIfNeeded())
  }

  componentDidMount() {
    this.fetchExhibitions()
  }

  render() {
    const { exhibitions, children } = this.props
    if (!exhibitions.length) {
      return <Spinner />
    }
    if (exhibitions.error) {
      return <ErrorView status={exhibitions.error.status} />
    }

    return (
      <DocumentTitle title="Exhibitions">
        <span>
          <ExhibitionIndexView exhibitions={exhibitions} />
          {children}
        </span>
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state, props) => ({
  exhibitions: selectors.getExhibitions(state),
})

export default connect(mapStateToProps)(ExhibitionIndexViewContainer)
