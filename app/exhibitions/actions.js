/* @flow weak */
import fetchUrl from 'core/fetch'
import * as t from './actionTypes'
import * as selectors from './selectors'

/**
 * Sets the active exhibition year
 */
export const setActiveYear = (year) => ({
  type: t.SET_ACTIVE_YEAR,
  payload: parseInt(year, 10),
})

/**
 * Fetch exhibitions from the api
 */
export const fetchExhibitions = () => async (dispatch) => {
  try {
    dispatch({ type: t.FETCH_EXHIBITIONS_REQUEST })
    const data = await fetchUrl('exhibitions/exhibition')

    // Set the active exhibition year
    if (data && data.length) {
      dispatch(setActiveYear(data[0].year))
    }

    dispatch({
      type: t.FETCH_EXHIBITIONS_SUCCESS,
      payload: data,
    })
  }
  catch (error) {
    dispatch({
      type: t.FETCH_EXHIBITIONS_FAILURE,
      payload: { error: { status: error.status } },
    })
  }
}

/**
 * Fetch exhibitions, but only if it is not available in state
 */
export const fetchExhibitionsIfNeeded = () => async (dispatch, getState) => {
  const existing = selectors.getExhibitions(getState())
  if (!existing || !existing.length) {
    dispatch(fetchExhibitions())
  }
}
