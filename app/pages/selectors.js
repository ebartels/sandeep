/* @flow weak */
import { createSelector } from 'reselect'

/**
 * Returns the state atom for pages module
 */
export const getState = state => state.pages


/**
 * Return state atom for all Pages
 */
export const getPageState = state => getState(state).pages


/**
 * Returns the page slug from state
 * This will be in props or params
 */
export const getPageSlug = (state, props) => (
  props.page_slug || (props.params && props.params.page_slug)
)

/**
 * Returns page data (including fetch state)
 */
export const getPageData = createSelector(
  [getPageState, getPageSlug],
  (state, slug) => state[slug]
)


/**
 * Returns page based on given slug
 */
export const getPage = createSelector(
  [getPageData, getPageSlug],
  (data, slug) => (
    (data && !data.isFetching) ? data : null
  )
)
