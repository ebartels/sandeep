/* @flow weak */
import React from 'react'
import { connect } from 'react-redux'
import ErrorView from 'ui/views/ErrorView'
import Spinner from 'ui/components/Spinner'
import DocumentTitle from 'ui/components/DocumentTitle'
import Markup from 'ui/components/Markup'
import { fetchPageIfNeeded } from 'pages/actions'
import { selectors } from 'pages'

type Props = {
  page: Object,
}

export const PageView = ({ page }: Props) => (
  <div id="page-view" className="page-view">
    <Markup>{page.text}</Markup>
  </div>
)

type ContainerProps = {
  page: Object,
  page_slug: string,
  dispatch: Function,
}

export class PageViewContainer extends React.Component {
  props: ContainerProps

  fetchPage(slug) {
    this.props.dispatch(fetchPageIfNeeded(slug))
  }

  componentDidMount() {
    this.fetchPage(this.props.page_slug)
  }

  componentWillReceiveProps(newProps) {
    if (newProps.page_slug !== this.props.page_slug) {
      this.fetchPage(newProps.page_slug)
    }
  }

  render() {
    const { page } = this.props
    if (!page) {
      return <Spinner />
    }
    if (page.error) {
      return <ErrorView status={page.error.status} />
    }
    return (
      <DocumentTitle title={page.title}>
        <PageView page={page} />
      </DocumentTitle>
    )
  }
}

const mapStateToProps = (state, props) => ({
  page: selectors.getPage(state, props),
  page_slug: selectors.getPageSlug(state, props),
})

export default connect(mapStateToProps)(PageViewContainer)
