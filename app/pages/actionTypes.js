/* @flow */
export const FETCH_PAGE_REQUEST = 'pages/FETCH_PAGE_REQUEST'
export const FETCH_PAGE_SUCCESS = 'pages/FETCH_PAGE_SUCCESS'
export const FETCH_PAGE_FAILURE = 'pages/FETCH_PAGE_FAILURE'
