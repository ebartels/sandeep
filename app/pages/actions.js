/* @flow weak */
import fetchUrl from 'core/fetch'
import * as t from './actionTypes'
import * as selectors from './selectors'

/**
 * Fetch a page by slug from the api
 */
export const fetchPage = (slug) => async (dispatch) => {
  try {
    dispatch({ type: t.FETCH_PAGE_REQUEST, payload: slug })
    const data = await fetchUrl(`pages/page/${slug}`)
    dispatch({
      type: t.FETCH_PAGE_SUCCESS,
      payload: data,
    })
  }
  catch (error) {
    dispatch({
      type: t.FETCH_PAGE_FAILURE,
      payload: { slug, error: { status: error.status } },
    })
  }
}

/**
 * Fetch a page by slug, but only if it is not available in state
 */
export const fetchPageIfNeeded = (slug) => async (dispatch, getState) => {
  const existing = selectors.getPageData(getState(), { page_slug: slug })
  if (!existing && !(existing || {}).isFetching) {
    dispatch(fetchPage(slug))
  }
}
