/* @flow weak */
import { combineReducers } from 'redux'
import * as t from './actionTypes'

export function pages(state = {}, action) {
  switch (action.type) {
    case t.FETCH_PAGE_REQUEST: {
      return {
        ...state,
        [action.payload]: {
          isFetching: true,
        },
      }
    }
    case t.FETCH_PAGE_SUCCESS: {
      return {
        ...state,
        [action.payload.slug]: action.payload,
      }
    }
    case t.FETCH_PAGE_FAILURE: {
      return {
        ...state,
        [action.payload.slug]: { error: action.payload.error },
      }
    }
    default:
      return state
  }
}

export default combineReducers({
  pages,
})
