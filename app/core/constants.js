/* @flow */
export const API_ROOT = '/api/'
export const DOCUMENT_TITLE = (document && document.title) || ''
export const MOBILE_MAX_WIDTH = 767
