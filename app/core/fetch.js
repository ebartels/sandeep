/* @flow weak */
import { API_ROOT } from 'core/constants'

class ApiError extends Error {
  status: number
  message: string
  data: Object

  constructor({ status, message, data }) {
    super(message)
    this.status = status
    this.message = message
    this.data = data
  }
}

const fetchUrl = async (url: string) => {
  const apiUrl = API_ROOT + url
  const response = await fetch(apiUrl)
  if (response.status >= 200 && response.status < 300) {
    return await response.json()
  }

  let data = {}
  try {
    data = await response.json()
  }
  catch (error) {
    // pass
  }

  throw new ApiError({
    data,
    status: response.status,
    text: response.statusText,
  })
}

export default fetchUrl
