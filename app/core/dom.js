/* @flow weak */
import jump from 'jump.js'
import { MOBILE_MAX_WIDTH } from 'core/constants'

export function isMobile() {
  return window.innerWidth <= MOBILE_MAX_WIDTH
}

export function scrollToElement(arg, opt = {}) {
  const el = typeof arg === 'string'
    ? document.querySelector(arg)
    : arg

  if (!el) {
    return
  }

  // Get offset (depends on whether mobile view or not)
  let offset = opt.offset || 0
  if (!offset && isMobile()) {
    offset = 1
  }

  // animated scroll
  if (opt && opt.duration) {
    jump(el, { ...opt, offset })
  }
  // direct scroll
  else {
    const bbox = el.getBoundingClientRect(el)
    const y = bbox.top + offset
    window.scrollBy(0, y)
  }
}
